import { withNextVideo } from 'next-video/process'
/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  env: {
    HOST_API: 'http://192.168.100.18:8080/api/admin',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/restaurants',
        permanent: false,
      },
    ]
  },
  images: {
    domains: ['localhost'],
  },
}

export default withNextVideo(nextConfig)
