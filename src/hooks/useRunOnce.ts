import { useState, useEffect } from 'react'
interface USeRunOnceProps {
  props?: any
  func: () => void
}
const useRunOnce = ({ props, func }: USeRunOnceProps) => {
  const [state, setState] = useState(false)
  let arr: any = []
  if (props?.dependencies) {
    arr = props?.dependencies
  }
  const runOnceFunc = () => {
    if (!state) {
      setState(true)
      func()
    }
  }
  useEffect(() => {
    if (arr.some((x: any) => x !== null)) {
      func()
    }
  }, [arr])
  return runOnceFunc
}
export default useRunOnce
