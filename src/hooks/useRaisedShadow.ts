import { animate, MotionValue, useMotionValue } from 'framer-motion'
import { useEffect } from 'react'

const inactiveShadow = '0px 2px 4px 0px #A5A3AE4D'
const inactiveBorderColor = '1px solid var(--border-color)'
const inactiveTextColor = 'var(--border-color)'

export function useRaisedShadow(value: MotionValue<number>) {
  const boxShadow = useMotionValue(inactiveShadow)

  useEffect(() => {
    let isActive = false
    value.onChange((latest) => {
      const wasActive = isActive
      if (latest !== 0) {
        isActive = true
        if (isActive !== wasActive) {
          animate(boxShadow, '5px 5px 10px rgba(0,0,0,0.3)')
        }
      } else {
        isActive = false
        if (isActive !== wasActive) {
          animate(boxShadow, inactiveShadow)
        }
      }
    })
  }, [value, boxShadow])

  return boxShadow
}
export function useRaisedText(value: MotionValue<number>) {
  const color = useMotionValue(inactiveTextColor)

  useEffect(() => {
    let isActive = false
    value.onChange((latest) => {
      const wasActive = isActive
      if (latest !== 0) {
        isActive = true
        if (isActive !== wasActive) {
          animate(color, 'rgb(var(--primary-color-500)/1)')
        }
      } else {
        isActive = false
        if (isActive !== wasActive) {
          animate(color, inactiveTextColor)
        }
      }
    })
  }, [value, color])

  return color
}

export function useRaisedBorder(value: MotionValue<number>) {
  const border = useMotionValue(inactiveBorderColor)

  useEffect(() => {
    let isActive = false
    value.onChange((latest) => {
      const wasActive = isActive
      if (latest !== 0) {
        isActive = true
        if (isActive !== wasActive) {
          animate(border, '1px solid rgb(var(--primary-color-500)/1)')
        }
      } else {
        isActive = false
        if (isActive !== wasActive) {
          animate(border, inactiveBorderColor)
        }
      }
    })
  }, [value, border])

  return border
}
