const regExp = {
  website: {
    pattern:
      /^ (((https: \/\/)| (http: \/\/)){1})?(w{3}\.)?([a-z0-9])(.[a-z0-9]{1,})$/,
    message: "يرجى ادخال رابط موقع صحيح",
  },
  phone: {
    pattern: /^(?:(\+|00)963|0)9[0-9]{8}$/,
    message: "يرجى ادخال رقم هاتف صحيح",
  },
  insta: {
    pattern: /^((https:\/\/){1})?(w{3}\.)?(instagram)(.com)\/[0-9a-zA-Z]+\/?$/,
    message: "يرجى ادخال رابط موقع instagram صحيح",
  },
  facebook: {
    pattern: /^((https:\/\/){1})?(w{3}\.)?(facebook)(.com)\/[0-9a-zA-Z]+\/?$/,
    message: "يرجى ادخال رابط موقع facebook صحيح",
  },
  noSpace: {
    pattern: /^\S*$/,
    message: "يرجى عدم ادخال مسافات",
  },
};
export default regExp;
