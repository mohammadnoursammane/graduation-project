import * as Yup from "yup";
import regExp from "./regExp";

const validation = {
    string: (props:any) => generateYups({ type: "string", ...props }),
    date: (props:any) => generateYups({ type: "date", ...props }),
    email: (props:any) => generateYups({ type: "email", ...props }),
    number: (props:any) => generateYups({ type: "number", ...props }),
    array: (props:any) => generateYups({ type: "array", ...props })
};
export default validation;
const generateYups = ({ type, ...props }:any) => {
    let temp :any= Yup;
    if (type === "string") {
        temp = temp.string("يجب أن يكون الإدخال نصي");
    } else if (type === "email") {
        temp = temp.string().email("يرجى ادخال إيميل صالح");
    } else if (type === "number") {
        temp = temp.number("يجب أن يكون الإدخال رقمي");
    } else if (type === "array") {
        temp = temp.array().of(props.validationOne);
    } else if (type === "date") {
        temp = temp.date('يرجى ادخال قيمة تاريخ صحيحة').nullable('يرجى ادخال قيمة تاريخ صحيحة')
    }

    if (props.min || props.min === 0) {
        temp = temp.min(
            props.min,
            props.min === 0
                ? `القيمة يجب أن تكون أكبر أو يساوي الصفر`
                : `الحد الأدنى للإدخال ${props.min}`
        );
    }
    if (props.max) {
        temp = temp.max(props.max, `الحد الأعلى للإدخال ${props.max}`);
    }
    if (props.regExp) {
        temp = temp.matches(props.regExp.pattern, props.regExp.message);
    }
    if (props.equality) {
        temp = temp.oneOf(
            [Yup.ref(props.equality.field), null],
            props.equality.message
        );
    }
    if (props.requiredLabel) {
        temp = temp.required(`يرجى ادخال ${props.requiredLabel}`);
    }

    return temp;
};
