import clsx, { ClassValue } from 'clsx'
import { twMerge } from 'tailwind-merge'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export function multiSelectStudents(value: string[]) {
  if (value.find((item: string) => item === 'all')) {
    while (value[value.length - 1] === 'all' && value.length > 1) {
      value.shift()
    }
    if (value[0] === 'all' && value.length > 1) {
      value.shift()
    }
  }
  return value
}

export function getTimeFromSeconds(value: string) {
  let sec_num = parseInt(value, 10) // don't forget the second param
  let hours: string | number = Math.floor(sec_num / 3600)
  let minutes: string | number = Math.floor((sec_num - hours * 3600) / 60)
  let seconds: string | number = sec_num - hours * 3600 - minutes * 60

  if (hours < 10) {
    hours = '0' + hours
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (seconds < 10) {
    seconds = '0' + seconds
  }
  return hours + ':' + minutes + ':' + seconds
}
