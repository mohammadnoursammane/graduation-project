import { NavigationProps } from '@/types'

const navBarLists: NavigationProps[] = [
  {
    id: 0,
    titleLink: 'المطاعم',
    Icon: 'Reserve',
    link: '/restaurants',
    queryLink: '',
    roles: ['1','2'],
  },
  {
    id: 1,
    titleLink: 'الموظفين',
    Icon: 'TagUser',
    link: '/employees',
    queryLink: '',
    roles: ['1','2'],
  },
  {
    id: 2,
    titleLink: 'الإعلانات',
    Icon: 'TbBellRinging',
    link: '/ads',
    queryLink: '',
    roles: ['2'],
  },
  {
    id: 3,
    titleLink: 'الزبائن',
    Icon: 'FiUsers',
    link: '/customers',
    roles: ['2'],
    queryLink: '',
  },
  {
    id: 4,
    titleLink: 'مستخدمو اللوحة',
    Icon: 'PiUserCircleDuotone',
    link: '/owners',
    queryLink: '',
    roles: ['1'],
  },
  {
    id: 5,
    titleLink: 'الطلبيات',
    Icon: 'TbShoppingCart',
    roles: ['1','2'],
    link: '/orders',
    queryLink: '',
  },
]

export { navBarLists }
