'use client'
// react & next
// formik & form
import { Field } from 'formik'
// rsuite
import { DatePicker } from 'rsuite'
// other
import LayoutField from './LayoutField'
// date
import { format } from 'date-fns'
import { cn } from '@/utilities/functions'
type DateField = {
  name: string
  className?: string
  label: string
  required?: boolean
  [props: string]: any
  disabled?: boolean
  hasTime?: boolean
}
export default function DateField({
  label,
  name,
  hasTime = false,
  className,
  required,
  ...props
}: DateField) {
  return (
    <LayoutField required={required} label={label} name={name} key={props.key}>
      <Field name={name}>
        {({ form, field }: any) => {
          const { setFieldValue } = form
          const { value } = field
          return (
            <DatePicker
              format={hasTime ? 'yyyy-MM-dd HH:mm:ss' : 'yyyy-MM-dd'}
              {...field}
              {...props}
              value={value ? new Date(value) : value}
              // onChange={val => setFieldValue(name, val)}
              onChange={(date: Date) => {
                setFieldValue(
                  name,
                  format(
                    new Date(date),
                    hasTime ? 'yyyy-MM-dd HH:mm:ss' : 'yyyy-MM-dd',
                  ),
                )
              }}
              cleanable={props.cleanable || false}
              oneTap
              className={cn(
                'w-full rounded-lg border py-1 transition-all hover:!border-primary-500',
                className,
                { 'read-only': props.readOnly },
              )}
              locale={{
                today: 'اليوم',
                yesterday: 'الأمس',
              }}
            />
          )
        }}
      </Field>
    </LayoutField>
  )
}
