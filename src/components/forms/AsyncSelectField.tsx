'use client'
// react & next
// rsuite
import { InputPicker, TagPicker } from 'rsuite'
// formik & form
import { useField } from 'formik'
// other
import LayoutField from './LayoutField'
import { cn, multiSelectStudents } from '@/utilities/functions'
import { ReactNode } from 'react'

type Placement =
  | 'bottomStart'
  | 'bottomEnd'
  | 'topStart'
  | 'topEnd'
  | 'leftStart'
  | 'leftEnd'
  | 'rightStart'
  | 'rightEnd'
  | 'auto'
  | 'autoVerticalStart'
  | 'autoVerticalEnd'
  | 'autoHorizontalStart'
  | 'autoHorizontalEnd'
type AsyncSelectFieldProps = {
  label?: string
  onChange?: (value: any) => void
  multiple?: boolean
  error: any
  loading: boolean
  cleanable?: boolean
  labelKey?: string
  valueKey?: string
  options: {
    name: string
    id: string | number
  }[]
  hasConditionSelect?: string
  defaultValue?: string | string[] | undefined
  name: string
  [props: string]: any
  value?: string | any
  placeholder: string
  renderMenu?: ReactNode
  placement?: Placement
  className?: string
  selectedElement?: ReactNode
}

export default function AsyncSelectField({
  label,
  labelKey = 'name',
  valueKey = 'id',
  options = [],
  error,
  loading,
  cleanable = false,
  onSearchHandler,
  defaultValue = undefined,
  hasConditionSelect,
  multiple = false,
  placeholder,
  onChange,
  name,
  value,
  renderMenu,
  className,
  placement = 'autoVerticalEnd',
  selectedElement,
  ...props
}: AsyncSelectFieldProps) {
  const [field, meta, helpers] = useField({
    name: name,
  })
  const ComponentSelect = multiple ? TagPicker : InputPicker
  var data = defaultValue ? defaultValue : options

  return (
    <LayoutField label={label} name={name}>
      <ComponentSelect
        data={options}
        onSelect={(value) => {
          if (hasConditionSelect === 'students') {
            value = multiSelectStudents(value)
          }
          helpers.setValue(value)
          onChange?.(value)
        }}
        menuMaxHeight={200}
        cleanable={cleanable ? cleanable : false}
        onClean={(event) => {
          helpers.setValue(null)
        }}
        defaultValue={defaultValue}
        labelKey={labelKey ? labelKey : 'name'}
        valueKey={valueKey ? valueKey : 'id'}
        {...props}
        className={cn(
          'w-full rounded-lg border py-1 transition-all',
          className,
        )}
        cacheData={options}
        preventOverflow={true}
        locale={{
          noResultsText: 'لا يوجد نتائج',
          placeholder: placeholder,
          searchPlaceholder: 'جاري البحث',
          loading: 'جاري التحميل',
          emptyMessage: 'عذرا حدثت مشكلة',
          checkAll: 'اختر الكل',
          newItem: 'عنصر جديد',
          createOption: 'إضافة خيار',
        }}
        renderMenuItem={(label, items) => {
          return (
            <div className="flex items-center justify-between">
              {label
                ?.toString()
                .split('manyValues')
                .map((item, index) => <p key={index}>{item}</p>)}
            </div>
          )
        }}
        renderMenu={(menu) => {
          if (loading) {
            return (
              <p style={{ padding: 4, color: '#999', textAlign: 'center' }}>
                {error ? 'عذرا حدثت مشكلة ما' : 'جاري التحميل'}
              </p>
            )
          }
          return renderMenu ?? menu
        }}
        placement={placement}
      />
    </LayoutField>
  )
}
