'use-client'
// react & next
import React, { useState } from 'react'
// rsuite
import { InputPicker, TagPicker } from 'rsuite'
// formik & form
import { useField } from 'formik'
// other
import LayoutField from './LayoutField'

export default function SelectField({
  label,
  options,
  multiple,
  labelKey,
  valueKey,
  cleanable,
  defaultValue,
  onChange,
  required = false,
  ...props
}: any) {
  const [field, meta, helpers] = useField(props)
  const ComponentSelect = multiple ? TagPicker : InputPicker

  return (
    <LayoutField required={required} label={label} name={props.name}>
      <ComponentSelect
        labelKey={labelKey ? labelKey : 'name'}
        valueKey={valueKey ? valueKey : 'value'}
        value={field.value}
        onChange={(val) => {
          helpers.setValue(val)
          onChange && onChange(val)
        }}
        menuMaxHeight={200}
        data={options}
        cleanable={cleanable ? cleanable : false}
        onClean={() => helpers.setValue('')}
        onSelect={(value) => {
          helpers.setValue(value)
        }}
        placeholder=""
        className="py-[0.15rem] cursor-pointer"
        cacheData={options}
        preventOverflow={true}
        {...props}
        locale={{
          noResultsText: 'لا يوجد نتائج',
          placeholder: 'اختر',
        }}
        defaultValue={field.value}
      />
    </LayoutField>
  )
}
