// formik & form
import { Field } from 'formik'
type CheckBoxOrRadioFieldProps = {
  name: string
  value?: any
  type: 'checkbox' | 'radio'
  text: string
  [props: string]: any
}
export default function CheckBoxOrRadioField({
  name,
  value,
  type,
  text,
  ...props
}: CheckBoxOrRadioFieldProps) {
  return (
    <div className="mb-4 flex items-center gap-4">
      <label>
        <Field
          name={name}
          className="field-layout"
          type={type}
          value={value}
          {...props}
        ></Field>
        <span> {text} </span>
      </label>
    </div>
  )
}
