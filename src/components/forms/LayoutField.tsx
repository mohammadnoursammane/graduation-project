// formik & form
import { ErrorMessage } from 'formik'
// other
import CustomErrorMessage from './CustomErrorMessage'
import { ReactNode } from 'react'
interface layoutProps {
  label?: string
  name: string
  required?: boolean
  children: ReactNode
  [props: string]: any
}
export default function LayoutField({
  label,
  name,
  children,
  required = false,
  ...props
}: layoutProps) {
  return (
    <div className={`flex w-full flex-col gap-1`} {...props}>
      {label && (
        <label htmlFor={name} className={`text-base text-heading`}>
          {label}
          {required && '*'}
        </label>
      )}
      {children}
      <ErrorMessage name={name}>
        {(msg) => <CustomErrorMessage message={msg} />}
      </ErrorMessage>
    </div>
  )
}
