import { cn } from '@/utilities/functions'
import { Field } from 'formik'
import React from 'react'

type RadioFieldTabsProps = {
  tabs: { name: string; value: string }[]
  name: string
}
function RadioFieldTabs({ tabs, name }: RadioFieldTabsProps) {
  return (
    <div
      role="group"
      className="mb-4 flex w-full overflow-hidden rounded-t-md border-b bg-secondary-500/10"
    >
      {tabs.map((tab, index) => (
        <label key={index} className="relative w-full">
          <Field
            type="radio"
            className="peer absolute left-0 top-0 opacity-0"
            name={name}
            value={tab.value}
          />
          <span
            className={cn(
              'block w-full py-2 text-center transition-all hover:bg-secondary-500/20 hover:text-primary-500',
              {
                'peer-checked:border-b-2 peer-checked:border-b-primary-500 peer-checked:text-primary-500':
                  true,
              },
            )}
          >
            {tab.name}
          </span>
        </label>
      ))}
    </div>
  )
}

export default RadioFieldTabs
