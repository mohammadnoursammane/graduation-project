import { useState } from 'react'
import LayoutField from './LayoutField'
import { Field, useField } from 'formik'
import { RxUpload } from 'react-icons/rx'
import Image from 'next/image'
import { TbTrash } from 'react-icons/tb'
import { LuPencil } from 'react-icons/lu'

function ImageFieldPro({
  name,
  label = '',
  required = false,
  //   setFieldValue,
  defaultImage,
  asIcon = false,
  asAvatar = false,
  afterChangeFunc,
  afterRemoveFunc,
}: {
  name: string
  label?: string
  required?: boolean
  asIcon?: boolean
  defaultImage?: string
  asAvatar?: boolean
  afterRemoveFunc?: () => void
  afterChangeFunc?: () => void
}) {
  const [file, setFile] = useState<string>(defaultImage ? defaultImage : '')
  const [field, meta, helpers] = useField({ name: name, type: 'file' })
  return (
    <>
      <LayoutField name={name} label={label} required={required}>
        {!file && !asIcon && !asAvatar && (
          <div className="relative flex min-h-20 w-full cursor-pointer justify-center rounded-lg border border-dashed p-4 transition-all">
            <Field
              accept="image/*"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              defaultValue={file}
              onChange={(e: any) => {
                if (e.target.files[0]) {
                  const fileLoaded = URL.createObjectURL(e.target.files[0])
                  setFile(fileLoaded)
                  helpers.setValue(e.target.files[0] as string)
                  if (afterChangeFunc) {
                    afterChangeFunc()
                  }
                }
              }}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {!file && asIcon && (
          <div className="relative w-fit cursor-pointer">
            <Field
              accept="image/*"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              // defaultValue={file}
              onChange={(e: any) => {
                if (e.target.files[0]) {
                  const fileLoaded = URL.createObjectURL(e.target.files[0])
                  setFile(fileLoaded)
                  helpers.setValue(e.target.files[0] as string)
                  if (afterChangeFunc) {
                    afterChangeFunc()
                  }
                }
              }}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {file && !asAvatar && (
          <div className="flex items-center justify-between">
            <div className="relative flex">
              <Image
                alt={name}
                width={100}
                height={100}
                className="aspect-video w-5/12 object-cover"
                src={file}
              />
            </div>
            <div className="flex items-center gap-4">
              <TbTrash
                className="cursor-pointer text-danger-500"
                onClick={() => {
                  setFile('')
                  helpers.setValue('')
                  if (afterRemoveFunc) {
                    afterRemoveFunc()
                  }
                }}
                size={'1.5rem'}
              />
              <div className="relative cursor-pointer">
                <Field
                  accept="image/*"
                  className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
                  type="file"
                  name={'file'}
                  // defaultValue={file}
                  onChange={(e: any) => {
                    if (e.target.files[0]) {
                      const fileLoaded = URL.createObjectURL(e.target.files[0])
                      setFile(fileLoaded)
                      helpers.setValue(e.target.files[0] as string)
                      if (afterChangeFunc) {
                        afterChangeFunc()
                      }
                    }
                  }}
                />
                <div
                  className={
                    'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
                  }
                >
                  <RxUpload />
                </div>
              </div>
            </div>
          </div>
        )}
        {file && asAvatar && (
          <div className="flex items-center justify-center">
            <div className="relative flex justify-center items-center">
              <Image
                alt={name}
                width={100}
                height={100}
                className="aspect-square rounded-full object-cover"
                src={file}
              />
              <div className="absolute left-0 bottom-0 hover:text-primary-500 hover:drop-shadow-lg transition-all cursor-pointer">
                <Field
                  accept="image/*"
                  className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
                  type="file"
                  name={'file'}
                  // defaultValue={file}
                  onChange={(e: any) => {
                    if (e.target.files[0]) {
                      const fileLoaded = URL.createObjectURL(e.target.files[0])
                      setFile(fileLoaded)
                      helpers.setValue(e.target.files[0] as string)
                      if (afterChangeFunc) {
                        afterChangeFunc()
                      }
                    }
                  }}
                />
                <div
                  className={
                    'cursor-pointer items-center justify-center rounded-full bg-background p-3'
                  }
                >
                  <LuPencil />
                </div>
              </div>
            </div>
          </div>
        )}
      </LayoutField>
    </>
  )
}

export default ImageFieldPro
