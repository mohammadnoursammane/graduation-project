// formik & form
import { ErrorMessage } from 'formik'
// icons
import { MdErrorOutline } from 'react-icons/md'

export default function CustomErrorMessage({
  message,
}: {
  message: React.ReactNode
}) {
  return (
    <div className="-mb-2 flex items-end gap-1 py-1 text-xs text-red-500">
      <MdErrorOutline className="text-red-500" /> {message}
    </div>
  )
}
