import React, { Dispatch, SetStateAction, useState } from 'react'
import LayoutField from './LayoutField'
import { Field, FormikErrors, useField } from 'formik'
import { RxUpload } from 'react-icons/rx'
import Image from 'next/image'
import { StaticImport } from 'next/dist/shared/lib/get-img-props'
import { TbEye, TbTrash } from 'react-icons/tb'
import { ProjectConfig } from '@/config'
import Link from 'next/link'

function FileFieldPro({
  name,
  label = '',
  required = false,
  //   setFieldValue,
  defaultFile,
  asIcon = false,
  afterChangeFunc,
  afterRemoveFunc,
  setFileProperty,
}: {
  name: string
  label?: string
  required?: boolean
  asIcon?: boolean
  defaultFile?: string
  afterRemoveFunc?: () => void
  afterChangeFunc?: () => void
  setFileProperty: Dispatch<SetStateAction<{ PageCount: number }>>
}) {
  const [file, setFile] = useState<string>(defaultFile ? defaultFile : '')
  const [field, meta, helpers] = useField({ name: name, type: 'file' })
  return (
    <>
      <LayoutField name={name} label={label} required={required}>
        {!file && !asIcon && (
          <div className="relative flex min-h-20 w-full cursor-pointer justify-center rounded-lg border border-dashed p-4 transition-all">
            <Field
              accept="application/pdf"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              defaultValue={file}
              onChange={(e: any) => {
                if (e.target.files[0]) {
                  const reader = new FileReader()
                  const fileInfo = e.target.files[0]
                  if (fileInfo) {
                    reader.readAsBinaryString(e.target.files[0])
                    reader.onloadend = () => {
                      const result = reader.result as string
                      const matches =
                        result.match(/\/Type[\s]*\/Page[^s]/g) ?? [] // Use nullish coalescing operator to handle null case
                      const count = matches.length
                      setFileProperty({ PageCount: count })
                    }
                  }
                  const fileLoaded = URL.createObjectURL(e.target.files[0])
                  setFile(fileLoaded)
                  helpers.setValue(e.target.files[0] as string)
                  if (afterChangeFunc) {
                    afterChangeFunc()
                  }
                }
              }}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {!file && asIcon && (
          <div className="relative w-fit cursor-pointer">
            <Field
              accept="image/*"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              // defaultValue={file}
              onChange={(e: any) => {
                if (e.target.files[0]) {
                  const reader = new FileReader()
                  const fileInfo = e.target.files[0]
                  if (fileInfo) {
                    reader.readAsBinaryString(e.target.files[0])
                    reader.onloadend = () => {
                      const result = reader.result as string
                      const matches =
                        result.match(/\/Type[\s]*\/Page[^s]/g) ?? [] // Use nullish coalescing operator to handle null case
                      const count = matches.length
                      setFileProperty({ PageCount: count })
                    }
                  }
                  const fileLoaded = URL.createObjectURL(e.target.files[0])
                  setFile(fileLoaded)
                  helpers.setValue(e.target.files[0] as string)
                  if (afterChangeFunc) {
                    afterChangeFunc()
                  }
                }
              }}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {file && (
          <div className="flex w-full items-center justify-between">
            <div className="relative flex">
              <iframe
                allowFullScreen={true}
                className="aspect-video w-full object-cover"
                src={file ?? URL.createObjectURL(file as any)}
                key={file}
                title={file}
              ></iframe>
            </div>
            <div className="flex items-center gap-4">
              <TbTrash
                className="cursor-pointer text-danger-500"
                onClick={() => {
                  setFile('')
                  setFileProperty({ PageCount: 0 })
                  helpers.setValue('')
                  if (afterRemoveFunc) {
                    afterRemoveFunc()
                  }
                }}
                size={'1.5rem'}
              />
              <Link target='_blank' href={file ?? URL.createObjectURL(file as any)}>
                <TbEye
                  className="cursor-pointer text-primary-500"
                  onClick={() => {
                    setFile('')
                    setFileProperty({ PageCount: 0 })
                    helpers.setValue('')
                    if (afterRemoveFunc) {
                      afterRemoveFunc()
                    }
                  }}
                  size={'1.5rem'}
                />
              </Link>

              <div className="relative cursor-pointer">
                <Field
                  accept="image/*"
                  className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
                  type="file"
                  name={'file'}
                  // defaultValue={file}
                  onChange={(e: any) => {
                    if (e.target.files[0]) {
                      const reader = new FileReader()
                      const fileInfo = e.target.files[0]
                      if (fileInfo) {
                        reader.readAsBinaryString(e.target.files[0])
                        reader.onloadend = () => {
                          const result = reader.result as string
                          const matches =
                            result.match(/\/Type[\s]*\/Page[^s]/g) ?? [] // Use nullish coalescing operator to handle null case
                          const count = matches.length
                          setFileProperty({ PageCount: count })
                        }
                      }
                      const fileLoaded = URL.createObjectURL(e.target.files[0])
                      setFile(fileLoaded)
                      helpers.setValue(e.target.files[0] as string)
                      if (afterChangeFunc) {
                        afterChangeFunc()
                      }
                    }
                  }}
                />
                <div
                  className={
                    'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
                  }
                >
                  <RxUpload />
                </div>
              </div>
            </div>
          </div>
        )}
      </LayoutField>
    </>
  )
}

export default FileFieldPro
