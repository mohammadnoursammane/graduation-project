import React, { Dispatch, SetStateAction, useEffect, useState } from 'react'
import LayoutField from './LayoutField'
import { Field, useField } from 'formik'
import { RxUpload } from 'react-icons/rx'
import { TbTrash } from 'react-icons/tb'
import { getTimeFromSeconds } from '@/utilities/functions'

function VideoFieldPro({
  name,
  label = '',
  required = false,
  //   setFieldValue,
  defaultVideo,
  asIcon = false,
  afterChangeFunc,
  afterRemoveFunc,
  setVideoProperty,
}: {
  name: string
  label?: string
  required?: boolean
  asIcon?: boolean
  defaultVideo?: string
  afterRemoveFunc?: () => void
  afterChangeFunc?: () => void
  setVideoProperty: Dispatch<SetStateAction<{ duration: string }>>
}) {
  const [file, setFile] = useState<string>(defaultVideo ? defaultVideo : '')
  const [field, meta, helpers] = useField({ name: name, type: 'file' })
  const [streamingUrl, setStreamingUrl] = useState<string>('')
  useEffect(() => {
    if (defaultVideo) {
      fetch(defaultVideo)
        .then((response) => response.json())
        .then((data) => {
          setStreamingUrl(data.streamingUrl)
        })
    }
  }, [defaultVideo])

  const handleFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0]) {
      const videoFile = e.target.files[0]
      const videoUrl = URL.createObjectURL(videoFile)
      const videoElement = document.createElement('video')
      videoElement.preload = 'metadata'
      videoElement.src = videoUrl
      videoElement.onloadedmetadata = () => {
        window.URL.revokeObjectURL(videoUrl)
        setVideoProperty({
          duration: getTimeFromSeconds(videoElement.duration.toString()),
        })
        setFile(videoUrl)
        helpers.setValue(videoFile as any)
        if (afterChangeFunc) {
          afterChangeFunc()
        }
      }
    }
  }

  return (
    <>
      <LayoutField name={name} label={label} required={required}>
        {!file && !asIcon && (
          <div className="relative flex min-h-20 w-full cursor-pointer justify-center rounded-lg border border-dashed p-4 transition-all">
            <Field
              accept="video/*"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              defaultValue={file}
              onChange={handleFileChange}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {!file && asIcon && (
          <div className="relative w-fit cursor-pointer">
            <Field
              accept="image/*"
              className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
              type="file"
              name={'file'}
              // defaultValue={file}
              onChange={handleFileChange}
            />
            <div
              className={
                'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
              }
            >
              <RxUpload />
            </div>
          </div>
        )}
        {(file || streamingUrl) && (
          <div className="flex items-center justify-between">
            <div className="relative flex">
              <video
                key={file || streamingUrl}
                className="aspect-video w-full object-cover"
                controls
                src={streamingUrl || file}
              />
            </div>
            <div className="flex items-center gap-4">
              <TbTrash
                className="cursor-pointer text-danger-500"
                onClick={() => {
                  setFile('')
                  helpers.setValue('')
                  if (afterRemoveFunc) {
                    afterRemoveFunc()
                  }
                }}
                size={'1.5rem'}
              />
              <div className="relative cursor-pointer">
                <Field
                  accept="image/*"
                  className="absolute left-0 top-0 h-full w-full cursor-pointer px-3 py-2 opacity-0 "
                  type="file"
                  name={'file'}
                  // defaultValue={file}
                  onChange={handleFileChange}
                />
                <div
                  className={
                    'cursor-pointer items-center justify-center rounded-md bg-background p-[14px] px-4'
                  }
                >
                  <RxUpload />
                </div>
              </div>
            </div>
          </div>
        )}
      </LayoutField>
    </>
  )
}

export default VideoFieldPro
