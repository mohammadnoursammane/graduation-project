'use client'
import { Field, ErrorMessage } from 'formik'
import classnames from 'classnames'
import { boolean } from 'yup'
import { useState } from 'react'
import { BiHide, BiShow } from 'react-icons/bi'
import LayoutField from './LayoutField'
import { cn } from '@/utilities/functions'
type TextFieldProps = {
  name: string
  type: string
  className?: string
  label: string
  unit?: string
  placeholder?: string
  isPassword?: boolean
  required?: boolean
  [props: string]: any
  disabled?: boolean
}
export default function TextField({
  name,
  type = 'text',
  className,
  label,
  unit,
  isPassword,
  required = false,
  disabled = false,
  ...props
}: TextFieldProps) {
  const [showPassword, setShowPassword] = useState(false)
  const hasAdornment = unit || isPassword
  const handleShowPassword = () => {
    setShowPassword((pre) => !pre)
  }
  return (
    <LayoutField required={required} label={label} name={name} key={props.key}>
      <Field
        name={name}
        type={showPassword ? 'text' : type}
        {...props}
        disabled={disabled}
        className={cn(
          'w-full rounded-lg border px-3 py-2 transition-all',
          className,
        )}
      />
    </LayoutField>
  )
}
