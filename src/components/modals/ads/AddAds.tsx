import {
  useGetRestaurantsQuery
} from '@/services/restaurantApi'
import {
  AdFormValues,
  ModalStates
} from '@/types'
import validation from '@/utilities/custom_validation'
import { useState } from 'react'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'
import { AsyncSelectField, ImageFieldPro } from '@/components/forms'
import { useAddAdMutation } from '@/services/adApi'

type AddAdsProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  refetch: () => void
}
function AddAds({ modalState, setModalState, title, refetch }: AddAdsProps) {
  const [addAd, {}] = useAddAdMutation()
  const onSubmitHandler = async (
    values: AdFormValues,
    {}: FormikHelpers<AdFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addAd({
        restaurant_id: values.restaurant_id,
        image: values.image,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      restaurant_id: validation.string({
        min: 2,
        requiredLabel: 'اسم المطعم',
      }),
    })
  }
  return (
    <ModalLayout
      open={modalState === 'add'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={{ image: '', restaurant_id: null }}
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({ isValid, isSubmitting }: FormikProps<AdFormValues>) => {
          // eslint-disable-next-line react-hooks/rules-of-hooks
          const [size, setSize] = useState<number>(1)
          const {
            data: restaurantData,
            isLoading: restaurantLoading,
            error: restaurantError,
            // eslint-disable-next-line react-hooks/rules-of-hooks
          } = useGetRestaurantsQuery({
            name: '',
            owner_name: '',
            perPage: size,
          })
          const onItemsRendered = (props: any) => {
            if (
              props.visibleStopIndex >=
              restaurantData!.data.restaurants.length - 1
            ) {
              setTimeout(() => {
                setSize(size + 0.5)
              }, 1000)
            }
          }
          const restaurantOptions = restaurantData?.data.restaurants.map(
            (item) => {
              return { id: item.id, name: item.name }
            },
          )
          return (
            <Form>
              <Modal.Body className="grid w-full gap-4">
                <ImageFieldPro name="image" label="إضافة صورة" />
                <AsyncSelectField
                  name="restaurant_id"
                  error={restaurantError}
                  loading={restaurantLoading}
                  options={restaurantOptions ?? []}
                placeholder='مطعم'
                  virtualized={true}
                  listProps={{
                    onItemsRendered,
                  }}
                  label="اسم المطعم"
                />
              </Modal.Body>
              <Modal.Footer className="flex w-full justify-center">
                <Button theme="primary" type="submit" isLoading={isSubmitting}>
                  إضافة
                </Button>
              </Modal.Footer>
            </Form>
          )
        }}
      />
    </ModalLayout>
  )
}

export default AddAds
