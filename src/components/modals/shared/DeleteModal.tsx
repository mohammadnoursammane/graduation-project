'use client'
import { Button, ModalLayout } from '@/components/ui/shared'
import { ModalStates } from '@/types'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'

type DeleteModalProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title: string
  id: string | number
  currentApiRoute: string
  deleteFun: (id: string | number) => void
  deleteEvents: { isLoading: boolean; isSuccess: boolean }
}
function DeleteModal({
  modalState,
  setModalState,
  title,
  id,
  deleteFun,
  deleteEvents,
}: DeleteModalProps) {
  const onSubmitHandler = async (
    values: { id: string | number },
    {}: FormikHelpers<{ id: string | number }>,
  ) => {
    deleteFun(values.id)
  }

  return (
    <ModalLayout
      open={modalState === 'delete'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik initialValues={{ id: id }} onSubmit={onSubmitHandler}>
        {({ isValid, isSubmitting }: FormikProps<{ id: string | number }>) => (
          <Form>
            <Modal.Body className="w-full text-center">
              <p>
                إن عملية حذف هذا العنصر سيسبب فقدانه بشكل نهائي ولا يمكن
                استعادته لاحقاً
              </p>
            </Modal.Body>
            <Modal.Footer className="flex w-full justify-center">
              <Button
                theme="danger"
                type="submit"
                isLoading={deleteEvents.isLoading}
              >
                تأكيد الحذف
              </Button>
            </Modal.Footer>
          </Form>
        )}
      </Formik>
    </ModalLayout>
  )
}

export default DeleteModal
