import {
  EmployeeData,
  ModalStates, EmployeeFormValues
} from '@/types'
import validation from '@/utilities/custom_validation'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'
import { ImageFieldPro, TextField } from '@/components/forms'
import {
  useAddEmployeeMutation,
  useEditEmployeeMutation,
} from '@/services/employeeApi'

type AddEditEmployeeProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  data?: EmployeeData | null
  refetch: () => void
}
function AddEditEmployee({
  modalState,
  setModalState,
  title,
  data = null,
  refetch,
}: AddEditEmployeeProps) {
  const [addEmployee, {}] = useAddEmployeeMutation()
  const [editEmployee, { isLoading: editLoading, isError: editError }] =
    useEditEmployeeMutation()
  const onSubmitHandler = async (
    values: EmployeeFormValues,
    {}: FormikHelpers<EmployeeFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addEmployee({
        name: values.name,
        password: values.password,
        password_confirmation: values.password_confirmation,
        image: values.image,
        phone_number: values.phone_number,
        restaurant_id: values.restaurant_id,
        username: values.username,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
    if (modalState === 'edit') {
      const result = await editEmployee({
        name: values.name,
        password: values.password,
        password_confirmation: values.password_confirmation,
        image: values.image,
        phone_number: values.phone_number,
        restaurant_id: values.restaurant_id,
        username: values.username,
        id: data?.id,
      })
      if ('data' in result) {
        if (result.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      name: validation.string({
        min: 2,
        requiredLabel: 'اسم الموظف',
      }),
    })
  }
  return (
    <ModalLayout
      open={modalState === 'add' || modalState === 'edit'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={
          modalState === 'edit' && data
            ? {
                name: data.name,
                password: '',
                password_confirmation: '',
                image: data.image,
                phone_number: data.phone_number,
                restaurant_id: 1,
                username: data.username,
                id: data.id,
              }
            : {
                name: '',
                password: '',
                password_confirmation: '',
                image: '',
                phone_number: '',
                restaurant_id: 1,
                username: '',
              }
        }
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({
          isValid,
          isSubmitting,
        }: FormikProps<EmployeeFormValues>) => (
          <Form>
            <Modal.Body className="grid w-full gap-4">
              <TextField
                required={true}
                label="اسم الموظف"
                name="name"
                type="text"
              />
              <TextField
                required={true}
                label="اسم المستخدم"
                name="username"
                type="text"
              />
              <ImageFieldPro
                name="image"
                label={modalState === 'add' ? 'إضافة صورة' : 'تعديل الصورة'}
                defaultImage={modalState === 'edit' ? data?.image : ''}
              />
              <TextField
                required={true}
                label="كلمة السر"
                name="password"
                type="password"
              />
              <TextField
                required={true}
                label="تأكيد كلمة السر"
                name="password_confirmation"
                type="password"
              />
              <TextField
                required={true}
                label="رقم الهاتف"
                name="phone_number"
                type="text"
              />
            </Modal.Body>
            <Modal.Footer className="flex w-full justify-center">
              <Button theme="primary" type="submit" isLoading={isSubmitting}>
                {modalState === 'add' ? 'إضافة' : 'تعديل'}
              </Button>
            </Modal.Footer>
          </Form>
        )}
      />
    </ModalLayout>
  )
}

export default AddEditEmployee
