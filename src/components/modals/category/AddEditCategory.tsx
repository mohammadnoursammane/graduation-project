'use client'
import { ModalStates, CategoryData, CategoryFormValues } from '@/types'
import validation from '@/utilities/custom_validation'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'
import { TextField } from '@/components/forms'
import {
  useAddCategoryMutation,
  useEditCategoryMutation,
} from '@/services/categoryApi'

type AddEditCategoryProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  data?: CategoryData | null
  refetch: () => void
  restaurantId: string | number
}
function AddEditCategory({
  modalState,
  setModalState,
  title,
  data = null,
  refetch,
  restaurantId,
}: AddEditCategoryProps) {
  const [addCategory, {}] = useAddCategoryMutation()
  const [editCategory, { isLoading: editLoading, isError: editError }] =
    useEditCategoryMutation()
  const onSubmitHandler = async (
    values: CategoryFormValues,
    {}: FormikHelpers<CategoryFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addCategory({
        name: values.name,
        restaurantId,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
    if (modalState === 'edit') {
      const result = await editCategory({
        name: values.name,
        id: data?.id,
      })
      if ('data' in result) {
        if (result.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      name: validation.string({
        min: 2,
        requiredLabel: 'اسم القسم',
      }),
    })
  }

  return (
    <ModalLayout
      open={modalState === 'add' || modalState === 'edit'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={
          modalState === 'edit' && data
            ? {
                name: data.name,
                id: data.id,
              }
            : {
                name: '',
              }
        }
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({
          isValid,
          isSubmitting,
        }: FormikProps<CategoryFormValues>) => {
          return (
            <Form>
              <Modal.Body className="grid w-full gap-4">
                <TextField
                  required={true}
                  label="اسم القسم"
                  name="name"
                  type="text"
                />
              </Modal.Body>
              <Modal.Footer className="flex w-full justify-center">
                <Button theme="primary" type="submit" isLoading={isSubmitting}>
                  {modalState === 'add' ? 'إضافة' : 'تعديل'}
                </Button>
              </Modal.Footer>
            </Form>
          )
        }}
      />
    </ModalLayout>
  )
}

export default AddEditCategory
