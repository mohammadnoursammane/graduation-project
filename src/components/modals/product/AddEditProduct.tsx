'use client'
import { ModalStates, ProductFormValues, ProductData } from '@/types'
import validation from '@/utilities/custom_validation'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal, Toggle } from 'rsuite'
import { ImageFieldPro, TextField } from '@/components/forms'
import {
  useAddProductMutation,
  useEditProductMutation,
} from '@/services/productApi'

type AddEditProductProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  data?: ProductData | null
  refetch: () => void
  restaurantId: string | number
  categoryId: string | number
}
function AddEditProduct({
  modalState,
  setModalState,
  title,
  data = null,
  refetch,
  restaurantId,
  categoryId,
}: AddEditProductProps) {
  const [addProduct, {}] = useAddProductMutation()
  const [editProduct, { isLoading: editLoading, isError: editError }] =
    useEditProductMutation()
  const onSubmitHandler = async (
    values: ProductFormValues,
    {}: FormikHelpers<ProductFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addProduct({
        name: values.name,
        restaurantId: restaurantId,
        details: values.details,
        categoryId: values.categoryId,
        image: values.image,
        is_shown: values.is_shown,
        price: values.price,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
    if (modalState === 'edit') {
      const result = await editProduct({
        name: values.name,
        restaurantId: restaurantId,
        details: values.details,
        categoryId: values.categoryId,
        image: values.image,
        is_shown: values.is_shown,
        price: values.price,
        id: values.id!,
      })
      if ('data' in result) {
        if (result.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      name: validation.string({
        min: 2,
        requiredLabel: 'اسم المنتج',
      }),
    })
  }

  return (
    <ModalLayout
      open={modalState === 'add' || modalState === 'edit'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={
          modalState === 'edit' && data
            ? {
                name: data.name,
                id: data.id,
                categoryId: categoryId,
                details: data.details,
                image: data.image,
                is_shown: data.is_shown,
                price: data.price,
                restaurantId: restaurantId,
              }
            : {
                name: '',
                categoryId: categoryId,
                details: '',
                image: '',
                is_shown: 0,
                price: 0,
                restaurantId: restaurantId,
              }
        }
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({
          isValid,
          isSubmitting,
          values,
          setFieldValue,
        }: FormikProps<ProductFormValues>) => {
          return (
            <Form>
              <Modal.Body className="grid w-full gap-4">
                <TextField
                  required={true}
                  label="اسم المنتج"
                  name="name"
                  type="text"
                />
                <ImageFieldPro
                  name="image"
                  label="إضافة صورة"
                  defaultImage={modalState === 'edit' ? data?.image : ''}
                />
                <TextField
                  required={true}
                  label="تفاصيل المنتج"
                  name="details"
                  type="text"
                  as="textarea"
                />
                <div className="flex items-center gap-4">
                  <Toggle
                    onChange={(checked) => {
                      setFieldValue('is_shown', checked ? 1 : 0)
                    }}
                    checked={values.is_shown === 1 ? true : false}
                  />
                  <p>يظهر هذا المنتج في التطبيق</p>
                </div>
                <TextField
                  required={true}
                  label="سعر المنتج"
                  name="price"
                  type="number"
                />
              </Modal.Body>
              <Modal.Footer className="flex w-full justify-center">
                <Button theme="primary" type="submit" isLoading={isSubmitting}>
                  {modalState === 'add' ? 'إضافة' : 'تعديل'}
                </Button>
              </Modal.Footer>
            </Form>
          )
        }}
      />
    </ModalLayout>
  )
}

export default AddEditProduct
