'use client'
import {
  useAddRestaurantMutation,
  useEditRestaurantMutation,
} from '@/services/restaurantApi'
import { ModalStates, RestaurantData, RestaurantFormValues } from '@/types'
import validation from '@/utilities/custom_validation'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'
import { AsyncSelectField, ImageFieldPro, TextField } from '@/components/forms'
import { useGetOwnersQuery } from '@/services/ownerApi'
import { useState } from 'react'

type AddEditRestaurantProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  data?: RestaurantData | null
  refetch: () => void
}
function AddEditRestaurant({
  modalState,
  setModalState,
  title,
  data = null,
  refetch,
}: AddEditRestaurantProps) {
  
  const [addSubject, {}] = useAddRestaurantMutation()
  const [editSubject, { isLoading: editLoading, isError: editError }] =
    useEditRestaurantMutation()
  const onSubmitHandler = async (
    values: RestaurantFormValues,
    {}: FormikHelpers<RestaurantFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addSubject({
        name: values.name,
        address: values.address,
        description: values.description,
        image: values.image,
        owner_id: values.owner_id,
        tax: values.tax,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
    if (modalState === 'edit') {
      const result = await editSubject({
        name: values.name,
        address: values.address,
        description: values.description,
        image: values.image,
        owner_id: values.owner_id,
        tax: values.tax,
        id: data?.id,
      })
      if ('data' in result) {
        if (result.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      name: validation.string({
        min: 2,
        requiredLabel: 'اسم المطعم',
      }),
    })
  }
  
  

  return (
    <ModalLayout
      open={modalState === 'add' || modalState === 'edit'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={
          modalState === 'edit' && data
            ? {
                name: data.name,
                address: data.address,
                description: data.description,
                image: data.image,
                owner_id: 1,
                tax: data.tax,
                id: data.id,
              }
            : {
                name: '',
                address: '',
                description: '',
                image: '',
                owner_id: null,
                tax: 0,
              }
        }
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({
          isValid,
          isSubmitting,
        }: FormikProps<RestaurantFormValues>) => 
        {
          // eslint-disable-next-line react-hooks/rules-of-hooks
          const [size, setSize] = useState<number>(0.5)
          const {
            data: ownerData,
            isLoading: ownerLoading,
            error: ownerError,
          // eslint-disable-next-line react-hooks/rules-of-hooks
          } = useGetOwnersQuery({ name: '', perPage: size })
          const onItemsRendered = (props: any) => {
            if (props.visibleStopIndex >= ownerData!.data.owners.length - 1) {
              setTimeout(() => {
                setSize(size + 0.5)
              }, 1000)
            }
          }
          const ownerOptions = ownerData?.data.owners.map((item) => {
            return { id: item.id, name: item.name }
          })
          return(
          <Form>
            <Modal.Body className="grid w-full gap-4">
              <TextField
                required={true}
                label="اسم المطعم"
                name="name"
                type="text"
              />
              <ImageFieldPro
                name="image"
                label="إضافة صورة"
                defaultImage={modalState === 'edit' ? data?.image : ''}
              />
              <TextField
                required={true}
                label="العنوان"
                name="address"
                as="textarea"
                type="text"
              />
              <TextField
                required={true}
                label="الوصف"
                name="description"
                type="text"
                as="textarea"
              />
              <TextField
                required={true}
                label="الضريبة"
                name="tax"
                type="number"
              />
              <AsyncSelectField
                name="owner_id"
                error={ownerError}
                loading={ownerLoading}
                options={ownerOptions ?? []}
                placeholder="صاحب المطعم"
                virtualized={true}
                listProps={{
                  onItemsRendered,
                }}
              />
            </Modal.Body>
            <Modal.Footer className="flex w-full justify-center">
              <Button theme="primary" type="submit" isLoading={isSubmitting}>
                {modalState === 'add' ? 'إضافة' : 'تعديل'}
              </Button>
            </Modal.Footer>
          </Form>
        )}}
      />
    </ModalLayout>
  )
}

export default AddEditRestaurant
