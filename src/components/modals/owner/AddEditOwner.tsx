import {
  ModalStates, OwnerRestaurantData,
  OwnerRestaurantFormValues
} from '@/types'
import validation from '@/utilities/custom_validation'
import * as Yup from 'yup'
import { Button, ModalLayout } from '@/components/ui/shared'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import { Modal } from 'rsuite'
import { ImageFieldPro, TextField } from '@/components/forms'
import { useAddOwnerMutation, useEditOwnerMutation } from '@/services/ownerApi'

type AddEditOwnerProps = {
  modalState: ModalStates
  setModalState: (val: ModalStates) => void
  title?: string
  data?: OwnerRestaurantData | null
  refetch: () => void
}
function AddEditOwner({
  modalState,
  setModalState,
  title,
  data = null,
  refetch,
}: AddEditOwnerProps) {
  const [addOwner, {}] = useAddOwnerMutation()
  const [editOwner, { isLoading: editLoading, isError: editError }] =
    useEditOwnerMutation()
  const onSubmitHandler = async (
    values: OwnerRestaurantFormValues,
    {}: FormikHelpers<OwnerRestaurantFormValues>,
  ) => {
    if (modalState === 'add') {
      const added = await addOwner({
        name: values.name,
        password: values.password,
        password_confirmation: values.password_confirmation,
        image: values.image,
        phone_number: values.phone_number,
        username: values.username,
      })
      if ('data' in added) {
        if (added.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
    if (modalState === 'edit') {
      const result = await editOwner({
        name: values.name,
        password: values.password,
        password_confirmation: values.password_confirmation,
        image: values.image,
        phone_number: values.phone_number,
        username: values.username,
        id: data?.id,
      })
      if ('data' in result) {
        if (result.data.success) {
          await refetch()
          setModalState(null)
        }
      }
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      name: validation.string({
        min: 2,
        requiredLabel: 'اسم المدير',
      }),
    })
  }
  return (
    <ModalLayout
      open={modalState === 'add' || modalState === 'edit'}
      title={title}
      handleClose={() => {
        setModalState(null)
      }}
    >
      <Formik
        initialValues={
          modalState === 'edit' && data
            ? {
                name: data.name,
                password: '',
                password_confirmation: '',
                image: data.image,
                phone_number: data.phone_number,
                username: data.username,
                id: data.id,
              }
            : {
                name: '',
                password: '',
                password_confirmation: '',
                image: '',
                phone_number: '',
                username: '',
              }
        }
        onSubmit={onSubmitHandler}
        validationSchema={onValidationHandler}
        component={({
          isValid,
          isSubmitting,
        }: FormikProps<OwnerRestaurantFormValues>) => (
          <Form>
            <Modal.Body className="grid w-full gap-4">
              <TextField
                required={true}
                label="اسم المدير"
                name="name"
                type="text"
              />
              <TextField
                required={true}
                label="اسم المستخدم"
                name="username"
                type="text"
              />
              <ImageFieldPro
                name="image"
                label={modalState === 'add' ? 'إضافة صورة' : 'تعديل الصورة'}
                defaultImage={modalState === 'edit' ? data?.image : ''}
              />
              <TextField
                required={true}
                label="كلمة السر"
                name="password"
                type="password"
              />
              <TextField
                required={true}
                label="تأكيد كلمة السر"
                name="password_confirmation"
                type="password"
              />
              <TextField
                required={true}
                label="رقم الهاتف"
                name="phone_number"
                type="text"
              />
            </Modal.Body>
            <Modal.Footer className="flex w-full justify-center">
              <Button theme="primary" type="submit" isLoading={isSubmitting}>
                {modalState === 'add' ? 'إضافة' : 'تعديل'}
              </Button>
            </Modal.Footer>
          </Form>
        )}
      />
    </ModalLayout>
  )
}

export default AddEditOwner
