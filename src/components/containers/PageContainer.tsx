'use client'
import React, { useState } from 'react'
import { Button } from '../ui/shared'
import { CiFilter } from 'react-icons/ci'
import { cn } from '@/utilities/functions'
import { TableProps } from '@/types'
import { Table } from '../ui/layout'
import Link from 'next/link'
import { IoSearch } from 'react-icons/io5'
import { Breadcrumb } from 'rsuite'

type PageContainerProps = {
  addFunction?: {
    children: React.ReactNode
    click?: () => void
    hide: boolean
  }
  extraFunction?: { children: React.ReactNode; click?: () => void }
  children?: React.ReactNode
  filterComponent?: React.ReactNode
  table?: TableProps
  pageTabs?: {
    pageTitle: string
    pageLink: string
    queryLink?: string
    active: boolean
  }[]
  searchComponent?: {
    setWord: (val: string) => void
    // submitHandler: () => void
  }
  breadcrumb?: { title: string; href?: string }[]
  reorderFunction?: { click: () => void }
}
function PageContainer({
  addFunction,
  children,
  filterComponent,
  table,
  pageTabs,
  searchComponent,
  breadcrumb,
  reorderFunction,
  extraFunction,
}: PageContainerProps) {
  const [filterState, setFilterState] = useState(false)
  return (
    <main className="container mx-auto py-9">
      <div className="flex w-full items-center justify-between">
        {breadcrumb && (
          <Breadcrumb className="flex-shrink-0 text-lg text-heading">
            {breadcrumb.map((item, index) =>
              item.href ? (
                <Breadcrumb.Item key={index} href={item.href}>
                  {item.title}
                </Breadcrumb.Item>
              ) : (
                <Breadcrumb.Item className="font-semibold" key={index} active>
                  {item.title}
                </Breadcrumb.Item>
              ),
            )}
          </Breadcrumb>
        )}

        <div className="mb-4 flex w-full justify-end gap-4">
          {searchComponent && (
            <div className="group flex items-center rounded-md border border-dark-500 bg-transparent text-dark-500">
              <label htmlFor="search">
                <IoSearch size={'2.5rem'} className="rounded-s-md px-2" />
              </label>
              <input
                name="search"
                id="search"
                onChange={(event) => {
                  setTimeout(() => {
                    searchComponent.setWord(event.target.value)
                  }, 1000)
                }}
                type="text"
                className=" w-12 rounded-e-md bg-transparent px-2 py-2 transition-all placeholder:text-dark-500 focus:w-48 focus:outline-none"
                placeholder="بحث"
              />
            </div>
          )}
          {extraFunction && (
            <Button
              onClick={extraFunction.click}
              theme="dark"
              styleType="outline"
            >
              {extraFunction.children}
            </Button>
          )}
          {reorderFunction && (
            <Button
              theme="dark"
              onClick={reorderFunction.click}
              styleType="outline"
            >
              <CiFilter size={'1.5rem'} />
              <p>تعديل الترتيب</p>
            </Button>
          )}
          {filterComponent && (
            <Button
              theme="dark"
              onClick={() => setFilterState((prop) => !prop)}
              styleType="outline"
            >
              <CiFilter size={'1.5rem'} />
              <p>فلترة</p>
            </Button>
          )}
          {addFunction && !addFunction.hide && (
            <Button onClick={addFunction.click} theme="primary">
              {addFunction.children}
            </Button>
          )}
        </div>
      </div>

      {filterState && filterComponent}
      {pageTabs && (
        <div className="mb-4 flex w-full overflow-hidden rounded-t-md border-b bg-secondary-500/10">
          {pageTabs.map((page, index) => (
            <Link
              key={index}
              className={cn(
                'w-full py-2 text-center transition-all hover:bg-secondary-500/20 hover:text-primary-500',
                {
                  'border-b-2 border-b-primary-500 text-primary-500':
                    page.active,
                },
              )}
              href={
                page.queryLink
                  ? page.pageLink + '?' + page.queryLink
                  : page.pageLink
              }
            >
              {page.pageTitle}
            </Link>
          ))}
        </div>
      )}
      {table && <Table table={table} />}

      <div className="">{children}</div>
    </main>
  )
}

export default PageContainer
