'use client'
import { FiSettings } from 'react-icons/fi'
import Link from 'next/link'
import { navBarLists } from '@/utilities/navigation/navbar'
import { usePathname } from 'next/navigation'
import { cn } from '@/utilities/functions'
import { useAppDispatch, useAppSelector } from '@/hooks/ReduxHooks'
import { useEffect, useRef } from 'react'
import { Dropdown } from 'rsuite'
import { getNavigationRoutes } from '@/lib/features/shared/sharedSlice'
import GetIconComponent from '@/lib/reactIcons'
import { CgProfile } from 'react-icons/cg'

function Navbar() {
  const dispatch = useAppDispatch()
  const { navigationRoutes } = useAppSelector((state) => state.shared)
  const subjectRef = useRef(false)
  const pathName = usePathname()
const {userRole}=useAppSelector(state=>state.auth)
  useEffect(() => {
    if (subjectRef.current === false) {
      // dispatch(getSubjects())
      dispatch(getNavigationRoutes())
    }

    return () => {
      subjectRef.current = true
    }
  }, [dispatch])

  return (
    <nav className="sticky z-50 rounded-b-lg bg-white shadow-lg shadow-[#A5A3AE73]">
      <div className="flex items-center justify-between border-b px-7 py-3">
        <div className="flex items-center gap-4">
          {/* <Logo className="w-28" /> */}
          <h4 className="text-xl font-semibold text-heading">اسم المطعم </h4>
        </div>
        <div className="flex items-center gap-5">
          {userRole[0]==='1'&&<Link href={'/settings'}>
            <FiSettings size={'1.5rem'} />
          </Link>}
          <Link href={'/my-profile'}>
            <CgProfile size={'1.7rem'} />
          </Link>
        </div>
      </div>
      {navigationRoutes.length ? (
        <div
          className={cn(
            'flex flex-col flex-wrap gap-4 px-7 py-3 sm:flex-row sm:items-center',
            // { hidden: userRole === 'CodeGenerator' },
          )}
        >
          {navigationRoutes.map((navLink, index) => {
            const IconComponent = GetIconComponent(navLink.Icon)
            const canRoute = navLink.roles?.findIndex(
              (role) => role === userRole[0],
            )
            if (canRoute !== -1)
            return (
              <div
                className="flex w-full items-center gap-4 sm:w-auto"
                key={index}
              >
                {navLink.title ? (
                  <Dropdown
                    title={navLink.title}
                    toggleClassName={cn('', {
                      ' hover:bg-gradient-to-tr hover:from-primary-500 hover:to-primary-300 bg-gradient-to-tr from-primary-500 to-primary-300 !rounded !text-white shadow shadow-primary-500 hover:drop-shadow-lg':
                        pathName.startsWith(navLink.link!),
                    })}
                    className={cn('gap-4')}
                    icon={<IconComponent size={'1.5rem'} />}
                  >
                    {navLink.list!.map((navLink1, index2) => (
                      <Dropdown.Item key={`${index}-${index2}`}>
                        <Link
                          href={
                            navLink1.queryLink
                              ? navLink1.link! + '?' + navLink1.queryLink
                              : navLink1.link!
                          }
                          className={cn(
                            'flex w-full items-center gap-4 rounded-lg p-3 transition-all hover:text-primary-500 sm:w-auto',
                            {
                              'font-semibold !text-primary-500':
                                pathName === navLink1.link,
                            },
                          )}
                        >
                          <p>{navLink1.titleLink}</p>
                        </Link>
                      </Dropdown.Item>
                    ))}
                  </Dropdown>
                ) : (
                  <Link
                    href={
                      navLink.queryLink
                        ? navLink.link! + '?' + navLink.queryLink
                        : navLink.link!
                    }
                    className={cn(
                      'flex w-full items-center gap-4 rounded-lg p-3 transition-all hover:text-primary-500 sm:w-auto',
                      {
                        'bg-gradient-to-tr from-primary-500 to-primary-300 !text-white shadow shadow-primary-500 hover:drop-shadow-lg':
                          pathName.startsWith(navLink.link!),
                      },
                    )}
                  >
                    {navLink.Icon && <IconComponent size={'1.5rem'} />}
                    <p>{navLink.titleLink}</p>
                  </Link>
                )}
                {index < navBarLists.length - 1 && (
                  <span className="hidden h-10 w-[2px] flex-shrink-0 rounded-full bg-gray-300 md:block"></span>
                )}
              </div>
            )
          })}
        </div>
      ) : (
        <></>
      )}
    </nav>
  )
}

export default Navbar
