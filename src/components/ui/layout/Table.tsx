'use client'
// DataTable
import DataTable from 'react-data-table-component'
// other
import classnames from 'classnames'
// rsuite
import { Loader } from 'rsuite'
// icons
import { FaInfoCircle } from 'react-icons/fa'
import { TableProps } from '@/types'
import { cn } from '@/utilities/functions'
type DataTableProps = {
  table: TableProps
  [props: string]: any
}
const Table = ({ table, ...props }: DataTableProps) => {
  return (
    <DataTable
      noHeader
      selectableRows={props.selectedRowsActionsItems ? true : false}
      responsive={true}
      columns={table.columns}
      data={table.data}
      progressComponent={
        <div
          className={classnames(
            'flex h-[400px] w-full items-center justify-center gap-4 rounded-lg bg-background p-2  text-2xl text-gray-400',
          )}
        >
          <h3 className="py-12 text-center font-semibold">
            جار تحميل البيانات <Loader />
          </h3>
        </div>
      }
      className='!rounded-none'
      customStyles={{
        table: {
          style: {
            background: 'transparent',
            borderRadius:'none',
            border: table.data.length === 0 ? 'none' : '1px solid #dbdade',
          },
        },
      }}
      progressPending={table.loading}
      noDataComponent={
        <div
          className={classnames(
            {
              'border-t-0': props.pageTabs,
            },
            'flex h-[400px] w-full items-center justify-center gap-4 rounded-lg border-none bg-background  p-2 text-2xl text-gray-400',
          )}
        >
          <FaInfoCircle />
          <h3 className="py-12 text-center font-semibold">لا يوجد بيانات</h3>
        </div>
      }
    />
  )
}
export default Table
