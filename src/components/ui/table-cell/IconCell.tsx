// react & next
import Link from 'next/link'
// rsuite
import { IconButton, Whisper, Popover } from 'rsuite'
// types
import { ThemeColors } from '@/types'
import { IconType } from 'react-icons'
// other
import { cn } from '@/utilities/functions'
type IconCellProps = {
  className?: string
  theme: ThemeColors
  title?: string
  href?: string
  icon: IconType
  [props: string]: any
}
function IconCell({
  className,
  theme = 'primary',
  icon: Icon,
  title,
  href,
  ...props
}: IconCellProps) {
  return href ? (
    <Link href={href} passHref={props.target === '_blank' ? true : false}>
      <Whisper
        placement="top"
        trigger="hover"
        speaker={
          <Popover className="px-3 py-2 !shadow-sm" arrow={false}>
            {title}
          </Popover>
        }
      >
        <IconButton
          circle
          className={cn(
            'flex w-full items-center justify-center rounded-full !p-1 sm:text-xl',
            className,
            {
              'text-primary-500 hover:bg-primary-100 hover:text-primary-600':
                theme === 'primary',
              'text-danger-500 hover:bg-danger-100 hover:text-danger-600':
                theme === 'danger',
              'text-success-500 hover:bg-success-100 hover:text-success-600':
                theme === 'success',
              'text-semiDark hover:bg-semiDark/20 hover:text-semiDark':
                theme === 'semiDark',
            },
          )}
          {...props}
          aria-label={title}
        >
          <Icon />
        </IconButton>
      </Whisper>
    </Link>
  ) : title ? (
    <Whisper
      placement="top"
      trigger="hover"
      speaker={
        <Popover className="px-3 py-2 !shadow-sm" arrow={false}>
          {title}
        </Popover>
      }
    >
      <IconButton
        circle
        className={cn(
          'flex w-full items-center justify-center rounded-full !p-1 sm:text-xl',
          className,
          {
            'text-primary-500 hover:bg-primary-100 hover:text-primary-600':
              theme === 'primary',
            'text-danger-500 hover:bg-danger-100 hover:text-danger-600':
              theme === 'danger',
            'text-success-500 hover:bg-success-100 hover:text-success-600':
              theme === 'success',
            'text-semiDark hover:bg-semiDark/20 hover:text-semiDark':
              theme === 'semiDark',
          },
        )}
        {...props}
        aria-label={title}
      >
        <Icon />
      </IconButton>
    </Whisper>
  ) : (
    <IconButton
      circle
      className={cn(
        'flex w-full items-center justify-center rounded-full !p-1 sm:text-xl',
        className,
        {
          'text-primary-500 hover:bg-primary-100 hover:text-primary-600':
            theme === 'primary',
          'text-danger-500 hover:bg-danger-100 hover:text-danger-600':
            theme === 'danger',
          'text-success-500 hover:bg-success-100 hover:text-success-600':
            theme === 'success',
        },
      )}
      {...props}
      aria-label={title}
    >
      <Icon />
    </IconButton>
  )
}

export default IconCell
