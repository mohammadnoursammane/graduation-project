'use client'
import { StyleTypes, ThemeColors } from '@/types'
import { cn } from '@/utilities/functions'
import React from 'react'
import { Loader } from 'rsuite'

type ButtonProps = {
  theme?: ThemeColors
  styleType?: StyleTypes
  className?: string
  children: React.ReactNode
  isLoading?: boolean
  disabled?: boolean
  [props: string]: any
}
function Button({
  theme = 'primary',
  children,
  className,
  styleType = 'fill',
  isLoading = false,
  disabled = false,
  ...props
}: ButtonProps) {
  return (
    <button
      {...props}
      disabled={isLoading || disabled}
      className={cn(
        ' flex items-center justify-center gap-2 rounded-md px-4 py-2 transition-all hover:drop-shadow',
        className,
        {
          'bg-primary-500 text-white shadow-primary-500 hover:bg-primary-600':
            theme === 'primary',
          'bg-danger-500 text-white shadow-danger-500 hover:bg-danger-600':
            theme === 'danger',
          'bg-gradient-to-tr from-primary-500 to-primary-300 text-white shadow-primary-500':
            theme === 'gradient',
          'bg-success-500 text-white shadow-success-500 hover:bg-success-600':
            theme === 'success',
          'bg-dark-500 text-white shadow-dark-500 hover:bg-dark-600':
            theme === 'dark',
          'bg-transparent p-0 hover:bg-transparent': theme === 'transparent',
          'border border-primary-500 bg-transparent text-primary-500 hover:bg-primary-500 hover:text-white':
            styleType === 'outline' && theme === 'primary',
          'border border-success-500 bg-transparent text-success-500 hover:bg-success-500 hover:text-white':
            styleType === 'outline' && theme === 'success',
          'border border-danger-500 bg-transparent text-danger-500 hover:bg-danger-500 hover:text-white':
            styleType === 'outline' && theme === 'danger',
          'border border-dark-500 bg-transparent text-dark-500 hover:bg-dark-500 hover:text-white':
            styleType === 'outline' && theme === 'dark',

          '!bg-opacity-50 hover:bg-gray-400': isLoading || disabled,
        },
      )}
    >
      {children}
      {isLoading && <Loader />}
    </button>
  )
}

export default Button
