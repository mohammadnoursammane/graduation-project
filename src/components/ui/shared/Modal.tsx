import React from 'react'
import { Modal } from 'rsuite'
type ModalLayoutProps = {
  open: boolean
  handleClose: () => void
  title?: string
  children: React.ReactNode
}
function ModalLayout({ open, handleClose, title, children }: ModalLayoutProps) {
  return (
    <Modal open={open} onClose={handleClose}>
      <Modal.Header>
        {title && (
          <Modal.Title className="text-center font-bold text-heading">
            {title}
          </Modal.Title>
        )}
      </Modal.Header>
      {children}
    </Modal>
  )
}

export default ModalLayout
