'use client'
import React, { useState } from 'react'
import ModalLayout from './Modal'
import { Modal } from 'rsuite'
import Image from 'next/image'
import { ProjectConfig } from '@/config'
import { cn } from '@/utilities/functions'
type ImageViewProps = {
  image: string
  className?: string
}
function ImageView({ image, className }: ImageViewProps) {
  const [open, setOpen] = useState(false)

  return (
    <>
      <Image
        src={image}
        width={100}
        height={100}
        className={cn('h-auto aspect-video object-cover w-full cursor-pointer', className)}
        alt={image}
        onClick={() => setOpen(true)}
      />
      <ModalLayout
        open={open}
        handleClose={() => {
          setOpen(false)
        }}
      >
        <Modal.Body>
          <Image
            src={ image}
            width={100}
            height={100}
            className="h-auto aspect-video object-cover w-full"
            alt={image}
          />
        </Modal.Body>
      </ModalLayout>
    </>
  )
}

export default ImageView
