// other
import { ThemeColors } from '@/types'
import { cn } from '@/utilities/functions'
import { CSSProperties } from 'react'

type BadgeProps = {
  content: string
  theme: ThemeColors
  size: 'small' | 'large'
  [props: string]: any
}
export default function Badge({ content, theme, size, ...props }: BadgeProps) {
  return (
    <span
      {...props}
      style={
        {
          '--color-bg': `var(--${theme}-color-700)`,
          '--color-text': `white`,
        } as CSSProperties
      }
      className={cn(
        {
          'px-1 text-xs': size === 'small',
          'px-2.5 py-0.5 text-sm ': size !== 'small',
        },
        ' rounded font-medium [background:var(--color-bg)] [color:var(--color-text)] [word-break:keep-all]',
      )}
    >
      {content}
    </span>
  )
}
