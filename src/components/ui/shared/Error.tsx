import { cn } from '@/utilities/functions'
import React from 'react'

function Error() {
  return (
    <div
      className={cn(
        'flex h-[400px] w-full items-center justify-center gap-4 rounded-lg bg-background p-2  text-2xl text-gray-400',
      )}
    >
      <h3 className="py-12 text-center font-semibold">عذراً حدث خطأ ما</h3>
      <h3 className="py-12 text-center font-semibold">يرجى إعادة تحميل البيانات</h3>
    </div>
  )
}

export default Error