import { cn } from '@/utilities/functions'
import React from 'react'
import { Loader } from 'rsuite'

function Loading() {
  return (
    <div
      className={cn(
        'flex h-[400px] w-full items-center justify-center gap-4 rounded-lg bg-background p-2  text-2xl text-gray-400',
      )}
    >
      <h3 className="py-12 text-center font-semibold">
        جار تحميل البيانات <Loader />
      </h3>
    </div>
  )
}

export default Loading
