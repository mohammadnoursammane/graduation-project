'use client'
// date
import { formatRelative, format, addMinutes, intlFormat } from 'date-fns'
import { ar, enUS } from 'date-fns/locale'
// rsuite
import { Whisper, Popover } from 'rsuite'

export default function DateView({ value }: { value: Date }) {
  return (
    <Whisper
      placement="top"
      trigger="hover"
      speaker={
        <Popover arrow={false}>
          {intlFormat(
            new Date(value),
            {
              year: 'numeric',
              month: 'long',
              day: 'numeric',
            },
            {
              locale: 'ar',
            },
          )}
        </Popover>
      }
    >
      <div className="w-max cursor-pointer">
        {formatRelative(new Date(value), new Date(), { locale: ar })}
      </div>
    </Whisper>
  )
}
