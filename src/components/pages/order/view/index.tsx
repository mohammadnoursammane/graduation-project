'use client'
import { PageContainer } from '@/components/containers'
import { Table } from '@/components/ui/layout'
import { DateView } from '@/components/ui/shared'
import { useGetOrderQuery } from '@/services/orderApi'
import { OrderData } from '@/types'
import Image from 'next/image'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { FaMinus, FaPlus, FaRegDotCircle } from 'react-icons/fa'

function ViewOrder({ orderId }: { orderId: string }) {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()

  const { data, error, isLoading, refetch, isFetching } = useGetOrderQuery({
    id: orderId,
  })

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])

  const cols: TableColumn<OrderData>[] = [
    {
      id: 'id',
      name: 'رقم الطلبية',
      cell: (row) => <div>{row.id}</div>,
    },
    {
      id: 'created_at',
      name: 'تاريخ الانشاء',
      cell: (row) => <DateView value={row.created_at} />,
    },
    {
      id: 'user_name',
      name: 'صاحب الطلبية',
      cell: (row) => <div title={row.user_name}>{row.user_name}</div>,
    },
    {
      id: 'restaurant_name',
      name: 'اسم المطعم',
      cell: (row) => (
        <div title={row.restaurant_name}>{row.restaurant_name}</div>
      ),
    },
    {
      id: 'waiter_name',
      name: 'اسم الموظف',
      cell: (row) => <div title={row.waiter_name}>{row.waiter_name}</div>,
    },
    {
      id: 'status',
      name: 'حالة الطلبية',
      cell: (row) => (
        <div
          title={
            row.status === 1
              ? 'قيد التنفيذ'
              : row.status === 2
                ? 'جاهزة'
                : 'مرفوضة'
          }
        >
          {row.status === 1
            ? 'قيد التنفيذ'
            : row.status === 2
              ? 'جاهزة'
              : 'مرفوضة'}
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        breadcrumb={[
          { title: 'الطلبيات', href: '/orders' },
          { title: data?.data.order.id.toString() ?? '' },
        ]}
      >
        <div className="rounded-xl bg-white p-6 shadow-[0px_4px_12px_0px_rgba(41,41,41,0.16)]">
          <Table
            className="bg-white"
            table={{
              columns: cols,
              data: [data?.data.order] ?? [],

              loading: isLoading,
              error: error,
            }}
          />
        </div>
        <div className="mt-8 grid grid-cols-3 items-start gap-8">
          <div className="grid gap-6 rounded-xl bg-white p-6 shadow-[0px_4px_12px_0px_rgba(41,41,41,0.16)]">
            <div className="flex items-center justify-between">
              <div className="flex items-center gap-4">
                <FaRegDotCircle />
                <p>المجموع</p>
              </div>
              <p>{data?.data.order.price} ل.س</p>
            </div>
            {data?.data.order.coupon_discount && (
              <div className="flex items-center justify-between">
                <div className="flex items-center gap-4">
                  <FaRegDotCircle />
                  <p>خصم الكوبون</p>
                </div>
                <p>{data?.data.order.coupon_discount} ل.س</p>
              </div>
            )}
            <div className="flex items-center justify-between">
              <div className="flex items-center gap-4">
                <FaRegDotCircle />
                <p>ضريبة الدخل والطاقة</p>
              </div>
              <p>{data?.data.order.restaurant_tax} ل.س</p>
            </div>
            <div className="mt-4 flex items-center justify-between">
              <div className="flex items-center gap-4">
                <FaRegDotCircle />
                <p>عدد النقاط المحققة</p>
              </div>
              <p>{data?.data.order.total_points} نقطة</p>
            </div>

            <div className="mt-4 flex items-center justify-between font-semibold">
              <div className="flex items-center gap-4">
                <FaRegDotCircle />
                <p>التكلفة الاجمالية</p>
              </div>
              <p>{data?.data.order.total_points} ل.س</p>
            </div>
          </div>
          <div className="col-span-2 grid grid-cols-2 gap-8">
            {data?.data.order.products.map((product, index) => (
              <div
                className="flex w-full rounded-xl bg-white shadow-[0px_4px_12px_0px_rgba(41,41,41,0.16)]"
                key={index}
              >
                <Image
                  src={product.image}
                  alt={product.product_name}
                  width={100}
                  height={100}
                  className="aspect-video w-1/2 rounded-xl object-cover"
                />
                <div className="flex w-full flex-col gap-1 p-2">
                  <p className="font-semibold">{product.product_name}</p>
                  <p className="font-semibold text-primary-500">
                    {product.product_price} ل.س
                  </p>
                  <div className="flex w-full items-center justify-end gap-2">
                    <FaMinus className="text-primary-500 opacity-50" />
                    {product.product_count}
                    <FaPlus className="text-primary-500 opacity-50" />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </PageContainer>
    </>
  )
}

export default ViewOrder
