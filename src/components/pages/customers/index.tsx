'use client'
import { PageContainer } from '@/components/containers'
import AddEditCustomer from '@/components/modals/customer/AddEditCustomer'
import { DeleteModal } from '@/components/modals/shared'
import { ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import {
  useDeleteCustomerMutation,
  useGetCustomersQuery,
} from '@/services/customerApi'
import { CustomerData, ModalStates } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbIndentDecrease, TbTrash } from 'react-icons/tb'

function Customers() {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{
    name: string
  }>({
    name: '',
  })
  const { data, error, isLoading, refetch, isFetching } = useGetCustomersQuery({
    perPage: size,
    name: filter.name,
  })
  const [deleteCustomer, deleteEvents] = useDeleteCustomerMutation()
  const [selectedRow, setSelectedRow] = useState<CustomerData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<CustomerData>[] = [
    {
      id: 'image',
      name: 'صورة الزبون',
      cell: (row) => (
        <ImageView className="aspect-square w-16" image={row.image} />
      ),
    },
    {
      id: 'name',
      name: 'اسم الزبون',
      cell: (row) => <div title={row.name}>{row.name}</div>,
    },
    {
      id: 'username',
      name: 'معرف المستخدم',
      cell: (row) => <div title={row.username}>{row.username}</div>,
    },
    {
      id: 'phone_number',
      name: 'رقم موبايل الزبون',
      cell: (row) => <div title={row.phone_number}>{row.phone_number}</div>,
    },

    {
      id: '.total_points',
      name: 'عدد النقاط الحاصل عليها',
      cell: (row) => (
        <div title={row.total_points.toString()}>{row.total_points}</div>
      ),
    },
    // {
    //   id: 'is_shown',
    //   name: 'الظهور في التطبيق',
    //   cell: (row) => (
    //     <div title={row.is_shown === 1 ? 'متاح' : 'غير متاح'}>
    //       {row.is_shown !== 1 ? (
    //         <CloseSquare
    //           size="28"
    //           className={'text-danger-500'}
    //           variant="Bold"
    //         />
    //       ) : (
    //         <TickSquare
    //           size="28"
    //           className={'text-success-500'}
    //           variant="Bold"
    //         />
    //       )}
    //     </div>
    //   ),
    // },
    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
          <IconCell
            theme="semiDark"
            href={`/customers/${row.id}`}
            title="عرض طلبيات الزبون"
            icon={TbIndentDecrease}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة زبون جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.users ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteCustomer(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذه سيسبب فقدانه بشكل نهائي`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditCustomer
            modalState={modalState}
            refetch={refetch}
            setModalState={setModalState}
          ></AddEditCustomer>
        )}
      </PageContainer>
    </>
  )
}

export default Customers
