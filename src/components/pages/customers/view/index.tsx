'use client'
import { PageContainer } from '@/components/containers'
import { DateView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import { useGetOrdersUserQuery } from '@/services/orderApi'
import { OrderData } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { TbIndentDecrease } from 'react-icons/tb'

function OrdersCustomer({ userId }: { userId: string }) {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()

  const { data, error, isLoading, refetch, isFetching } = useGetOrdersUserQuery(
    {
      id: userId,
    },
  )

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<OrderData>[] = [
    {
      id: 'id',
      name: 'رقم الطلبية',
      cell: (row) => <div title={row.id.toString()}>{row.id}</div>,
    },
    {
      id: 'created_at',
      name: 'تاريخ الانشاء',
      cell: (row) => <DateView value={row.created_at} />,
    },
    {
      id: 'user_name',
      name: 'صاحب الطلبية',
      cell: (row) => <div title={row.user_name}>{row.user_name}</div>,
    },
    {
      id: 'restaurant_name',
      name: 'اسم المطعم',
      cell: (row) => (
        <div title={row.restaurant_name}>{row.restaurant_name}</div>
      ),
    },
    {
      id: 'waiter_name',
      name: 'اسم الموظف',
      cell: (row) => <div title={row.waiter_name}>{row.waiter_name}</div>,
    },
    {
      id: 'status',
      name: 'حالة الطلبية',
      cell: (row) => (
        <div
          title={
            row.status === 1
              ? 'قيد التنفيذ'
              : row.status === 2
                ? 'جاهزة'
                : 'مرفوضة'
          }
        >
          {row.status === 1
            ? 'قيد التنفيذ'
            : row.status === 2
              ? 'جاهزة'
              : 'مرفوضة'}
        </div>
      ),
    },

    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="semiDark"
            href={`/orders/${row.id}`}
            title="عرض تفاصيل الطلبية"
            icon={TbIndentDecrease}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        breadcrumb={[
          { title: 'الزبائن', href: '/customers' },
          { title: data?.data.orders[0].user_name ?? '' },
        ]}
        table={{
          columns: cols,
          data: data?.data.orders ?? [],

          loading: isLoading,
          error: error,
        }}
      ></PageContainer>
    </>
  )
}

export default OrdersCustomer
