'use client'
import { PageContainer } from '@/components/containers'
import AddEditEmployee from '@/components/modals/employee/AddEditEmployee'
import { DeleteModal } from '@/components/modals/shared'
import { ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import {
  useDeleteEmployeeMutation,
  useGetEmployeesQuery,
} from '@/services/employeeApi'
import { EmployeeData, ModalStates } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbTrash } from 'react-icons/tb'

function Employees() {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{
    restaurant_name: string
    name: string
  }>({
    restaurant_name: '',
    name: '',
  })
  const { data, error, isLoading, refetch, isFetching } = useGetEmployeesQuery({
    restaurant_name: filter.restaurant_name,
    perPage: size,
    name: filter.name,
  })
  const [deleteEmployee, deleteEvents] = useDeleteEmployeeMutation()
  const [selectedRow, setSelectedRow] = useState<EmployeeData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<EmployeeData>[] = [
    {
      id: 'image',
      name: 'صورة الموظف',
      cell: (row) => (
        <ImageView className="aspect-square w-16" image={row.image} />
      ),
    },
    // {
    //   id: 'name',
    //   name: 'اسم الموظف',
    //   cell: (row) => <div title={row.name}>{row.name}</div>,
    // },
    // {
    //   id: 'restaurant_name',
    //   name: 'اسم المطعم الذي يعمل فيه',
    //   cell: (row) => (
    //     <div title={row.restaurant_name}>{row.restaurant_name}</div>
    //   ),
    // },
    {
      id: 'phone_number',
      name: 'رقم موبايل الموظف',
      cell: (row) => <div title={row.phone_number}>{row.phone_number}</div>,
    },
    {
      id: 'username',
      name: 'معرف الموظف',
      cell: (row) => <div title={row.username}>{row.username}</div>,
    },
    {
      id: 'orders_count',
      name: 'عدد الطلبات المنجزة الإجمالي / يوم عمل',
      cell: (row) => (
        <div title={row.orders_count.toString()}>{row.orders_count}</div>
      ),
    },

    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة موظف جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.waiters ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteEmployee(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذه سيسبب فقدانه بشكل نهائي`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditEmployee
            modalState={modalState}
            refetch={refetch}
            setModalState={setModalState}
          ></AddEditEmployee>
        )}
      </PageContainer>
    </>
  )
}

export default Employees
