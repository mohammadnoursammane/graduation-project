'use client'
import { PageContainer } from '@/components/containers'
import AddEditOwner from '@/components/modals/owner/AddEditOwner'
import { DeleteModal } from '@/components/modals/shared'
import { ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import { useDeleteOwnerMutation, useGetOwnersQuery } from '@/services/ownerApi'
import { ModalStates, OwnerRestaurantData } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbTrash } from 'react-icons/tb'

function Owners() {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{
    name: string
  }>({
    name: '',
  })
  const { data, error, isLoading, refetch, isFetching } = useGetOwnersQuery({
    perPage: size,
    name: filter.name,
  })
  const [deleteOwner, deleteEvents] = useDeleteOwnerMutation()
  const [selectedRow, setSelectedRow] = useState<OwnerRestaurantData | null>(
    null,
  )
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<OwnerRestaurantData>[] = [
    {
      id: 'image',
      name: 'صورة المستخدم',
      cell: (row) => (
        <ImageView className="aspect-square w-16" image={row.image} />
      ),
    },
    {
      id: 'name',
      name: 'اسم المستخدم',
      cell: (row) => <div title={row.name}>{row.name}</div>,
    },

    {
      id: 'phone_number',
      name: 'رقم موبايل المستخدم',
      cell: (row) => <div title={row.phone_number}>{row.phone_number}</div>,
    },
    {
      id: 'username',
      name: 'معرف المستخدم',
      cell: (row) => <div title={row.username}>{row.username}</div>,
    },

    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة مدير جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.owners ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteOwner(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذه سيسبب فقدانه بشكل نهائي`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditOwner
            modalState={modalState}
            refetch={refetch}
            setModalState={setModalState}
          ></AddEditOwner>
        )}
      </PageContainer>
    </>
  )
}

export default Owners
