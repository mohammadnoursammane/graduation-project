'use client'
import { PageContainer } from '@/components/containers'
import AddEditProduct from '@/components/modals/product/AddEditProduct'
import { DeleteModal } from '@/components/modals/shared'
import { ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import {
  useDeleteProductMutation,
  useGetProductsQuery,
} from '@/services/productApi'
import { ProductData, ModalStates } from '@/types'
import { CloseSquare, TickSquare } from 'iconsax-react'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbTrash } from 'react-icons/tb'

function Products({
  restaurantId,
  categoryId,
}: {
  restaurantId: string
  categoryId: string
}) {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{
    name: string
    price: number
  }>({
    name: '',
    price: 0,
  })
  const { data, error, isLoading, refetch, isFetching } = useGetProductsQuery({
    name: filter.name,
    restaurantId: restaurantId,
    categoryId: categoryId,
    price: filter.price,
    perPage: size,
  })
  const [deleteProduct, deleteEvents] = useDeleteProductMutation()
  const [selectedRow, setSelectedRow] = useState<ProductData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<ProductData>[] = [
    {
      id: 'name',
      name: 'اسم المنتج',
      cell: (row) => <div title={row.name}>{row.name}</div>,
    },
    {
      id: 'image',
      name: 'صورة المنتج',
      cell: (row) => <ImageView image={row.image} />,
    },
    {
      id: 'details',
      name: 'تفاصيل المنتج',
      cell: (row) => <div title={row.details}>{row.details}</div>,
    },
    {
      id: 'price',
      name: 'سعر المنتج',
      cell: (row) => <div title={row.price.toString()}>{row.price}</div>,
    },
    {
      id: 'points',
      name: 'النقاط على المنتج',
      cell: (row) => <div title={row.points.toString()}>{row.points}</div>,
    },
    {
      id: 'is_shown',
      name: 'الظهور في التطبيق',
      cell: (row) => (
        <div title={row.is_shown === 1 ? 'متاح' : 'غير متاح'}>
          {row.is_shown !== 1 ? (
            <CloseSquare
              size="28"
              className={'text-danger-500'}
              variant="Bold"
            />
          ) : (
            <TickSquare
              size="28"
              className={'text-success-500'}
              variant="Bold"
            />
          )}
        </div>
      ),
    },
    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة منتج جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.products ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteProduct(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذا المنتج سيسبب فقدانه بشكل نهائي ولا يمكن استعادته لاحقاً`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditProduct
            modalState={modalState}
            refetch={refetch}
            restaurantId={restaurantId}
            data={modalState === 'edit' ? selectedRow : null}
            setModalState={setModalState}
            categoryId={categoryId}
          ></AddEditProduct>
        )}
      </PageContainer>
    </>
  )
}

export default Products
