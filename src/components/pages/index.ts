import Home from './Home'
import Profile from './profile'
import Orders from './order'
import ViewOrder from './order/view'
import Settings from './settings'
import Customers from './customers'
import Owners from './owners'
export { Home, Profile, Orders, ViewOrder, Settings, Customers, Owners }
