'use client'
import { PageContainer } from '@/components/containers'
import AddAds from '@/components/modals/ads/AddAds'
import { DeleteModal } from '@/components/modals/shared'
import { ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import { useDeleteAdMutation, useGetAdsQuery } from '@/services/adApi'
import { AdData, ModalStates } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { TbTrash } from 'react-icons/tb'

function Ads() {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{
    restaurant_name: string
    owner_name: string
  }>({
    restaurant_name: '',
    owner_name: '',
  })
  const { data, error, isLoading, refetch, isFetching } = useGetAdsQuery({
    restaurant_name: filter.restaurant_name,
    owner_name: filter.owner_name,
    perPage: size,
  })
  const [deleteAd, deleteEvents] = useDeleteAdMutation()
  const [selectedRow, setSelectedRow] = useState<AdData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<AdData>[] = [
    {
      id: 'image',
      name: 'صورة الإعلان',
      cell: (row) => <ImageView className='aspect-square w-16' image={row.image} />,
    },
    {
      id: 'restaurant_name',
      name: 'اسم المطعم',
      cell: (row) => (
        <div title={row.restaurant_name}>{row.restaurant_name}</div>
      ),
    },
    {
      id: 'restaurant_owner_name',
      name: 'اسم صاحب المطعم',
      cell: (row) => (
        <div title={row.restaurant_owner_name}>{row.restaurant_owner_name}</div>
      ),
    },
    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة إعلان جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.ad ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteAd(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذا المطعم سيسبب فقدانه بشكل نهائي بالإضافة إلى جميع أقسامه ومنتجاته ولا يمكن استعادته لاحقاً`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {modalState === 'add' && (
          <AddAds
            modalState={modalState}
            refetch={refetch}
            setModalState={setModalState}
          ></AddAds>
        )}
      </PageContainer>
    </>
  )
}

export default Ads
