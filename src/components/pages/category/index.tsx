'use client'
import { PageContainer } from '@/components/containers'
import AddEditCategory from '@/components/modals/category/AddEditCategory'
import { DeleteModal } from '@/components/modals/shared'
import { IconCell } from '@/components/ui/table-cell'
import {
  useDeleteCategoryMutation,
  useGetCategoriesQuery,
} from '@/services/categoryApi'
import { CategoryData, ModalStates } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbIndentDecrease, TbTrash } from 'react-icons/tb'

function Categories({ restaurantId }: { restaurantId: string }) {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{ name: string; owner_name: string }>({
    name: '',
    owner_name: '',
  })
  const { data, error, isLoading, refetch, isFetching } = useGetCategoriesQuery(
    {
      name: filter.name,
      restaurantId: restaurantId,
      perPage: size,
    },
  )
  const [deleteCategory, deleteEvents] = useDeleteCategoryMutation()
  const [selectedRow, setSelectedRow] = useState<CategoryData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<CategoryData>[] = [
    {
      id: 'name',
      name: 'اسم الصنف',
      cell: (row) => <div title={row.name}>{row.name}</div>,
    },
    // {
    //   id: 'image',
    //   name: 'صورة المطعم',
    //   cell: (row) => <ImageView image={row.image} />,
    // },
    {
      id: 'products_count',
      name: 'عدد المنتجات الكلي',
      cell: (row) => (
        <div title={row.products_count.toString()}>
          {row.products_count.toString()}
        </div>
      ),
    },
    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
          <IconCell
            theme="semiDark"
            href={`/restaurants/${restaurantId}/${row.id}`}
            title="عرض منتجات القسم"
            icon={TbIndentDecrease}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة قسم جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.categories ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteCategory(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذا القسم سيسبب فقدانه بشكل نهائي بالإضافة إلى جميع منتجاته ولا يمكن استعادته لاحقاً`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditCategory
            modalState={modalState}
            refetch={refetch}
            restaurantId={restaurantId}
            data={modalState === 'edit' ? selectedRow : null}
            setModalState={setModalState}
          ></AddEditCategory>
        )}
      </PageContainer>
    </>
  )
}

export default Categories
