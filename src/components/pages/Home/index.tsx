'use client'
import { PageContainer } from '@/components/containers'
import AddEditRestaurant from '@/components/modals/restaurant/AddEditRestaurant'
import { DeleteModal } from '@/components/modals/shared'
import { DateView, ImageView } from '@/components/ui/shared'
import { IconCell } from '@/components/ui/table-cell'
import {
  useDeleteRestaurantMutation,
  useGetRestaurantsQuery,
} from '@/services/restaurantApi'
import { ModalStates, RestaurantData } from '@/types'
import { useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'
import { TableColumn } from 'react-data-table-component'
import { LuPencil } from 'react-icons/lu'
import { TbIndentDecrease, TbTrash } from 'react-icons/tb'

function Home() {
  const [size, setSize] = useState<number>(1)
  const searchParams = useSearchParams()
  const [filter, setFilter] = useState<{ name: string; owner_name: string }>({
    name: '',
    owner_name: '',
  })
  const { data, error, isLoading, refetch, isFetching } =
    useGetRestaurantsQuery({
      name: filter.name,
      owner_name: filter.owner_name,
      perPage: size,
    })
  const [deleteRestaurant, deleteEvents] = useDeleteRestaurantMutation()
  const [selectedRow, setSelectedRow] = useState<RestaurantData | null>(null)
  const [modalState, setModalState] = useState<ModalStates>(null)

  useEffect(() => {
    const onScroll = () => {
      const scrolledToBottom =
        window.innerHeight + window.scrollY <= document.body.offsetHeight
      if (scrolledToBottom && !isFetching) {
        setSize(size + 1)
      }
    }

    document.addEventListener('scroll', onScroll)

    return function () {
      document.removeEventListener('scroll', onScroll)
    }
  }, [size, isFetching])
  const cols: TableColumn<RestaurantData>[] = [
    {
      id: 'name',
      name: 'اسم المطعم',
      cell: (row) => <div title={row.name}>{row.name}</div>,
    },
    {
      id: 'image',
      name: 'صورة المطعم',
      cell: (row) => <ImageView image={row.image} />,
    },
    {
      id: 'address',
      name: 'العنوان',
      cell: (row) => <div title={row.address}>{row.address}</div>,
    },
    {
      id: 'owner',
      name: 'اسم صاحب المطعم',
      cell: (row) => <div title={row.owner.name}>{row.owner.name}</div>,
    },
    {
      id: 'categories_count',
      name: 'عدد الأقسام',
      cell: (row) => (
        <div title={row.categories_count.toString()}>
          {row.categories_count.toString()}
        </div>
      ),
    },
    {
      id: 'products_count',
      name: 'عدد المنتجات الكلي',
      cell: (row) => (
        <div title={row.products_count.toString()}>
          {row.products_count.toString()}
        </div>
      ),
    },
    {
      id: 'created_at',
      name: 'تاريخ الإنشاء',
      cell: (row) => <DateView value={row.created_at}></DateView>,
    },

    {
      name: 'التفاصيل',
      width: '140px',
      cell: (row) => (
        <div className="flex justify-center gap-4">
          <IconCell
            theme="primary"
            title="تعديل"
            onClick={() => {
              setSelectedRow(row)
              setModalState('edit')
            }}
            icon={LuPencil}
          />
          <IconCell
            theme="danger"
            title="حذف"
            onClick={() => {
              setSelectedRow(row)
              setModalState('delete')
            }}
            icon={TbTrash}
          />
          <IconCell
            theme="semiDark"
            href={`/restaurants/${row.id}`}
            title="عرض أقسام المطعم"
            icon={TbIndentDecrease}
          />
        </div>
      ),
    },
  ]
  return (
    <>
      <PageContainer
        addFunction={{
          children: <p>إضافة مطعم جديد</p>,
          click: () => {
            setModalState((prev) => 'add')
          },
          hide: false,
        }}
        table={{
          columns: cols,
          data: data?.data.restaurants ?? [],

          loading: isLoading,
          error: error,
        }}
      >
        {modalState === 'delete' && (
          <DeleteModal
            currentApiRoute=""
            deleteEvents={deleteEvents}
            deleteFun={async (id) => {
              const deleted = await deleteRestaurant(id)
              if (deleted) {
                refetch()
                setModalState(null)
              }
            }}
            modalState={modalState}
            title={`إن عملية حذف هذا المطعم سيسبب فقدانه بشكل نهائي بالإضافة إلى جميع أقسامه ومنتجاته ولا يمكن استعادته لاحقاً`}
            id={selectedRow!.id}
            setModalState={setModalState}
          ></DeleteModal>
        )}
        {(modalState === 'add' || modalState === 'edit') && (
          <AddEditRestaurant
            modalState={modalState}
            refetch={refetch}
            data={modalState === 'edit' ? selectedRow : null}
            setModalState={setModalState}
          ></AddEditRestaurant>
        )}
      </PageContainer>
    </>
  )
}

export default Home
