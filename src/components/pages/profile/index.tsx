'use client'
import { ImageFieldPro, TextField } from '@/components/forms'
import { Button, Error, Loading } from '@/components/ui/shared'
import {
  useEditProfileMutation,
  useGetProfileQuery,
} from '@/services/profileApi'
import { ProfileFormValues } from '@/types'
import { Form, Formik, FormikProps } from 'formik'

function Profile() {
  const { data, isLoading, error } = useGetProfileQuery()
  const [editProfile, {}] = useEditProfileMutation()
  const onSubmitHandler = async (values: ProfileFormValues) => {
    const res = await editProfile({
      name: values.name,
      password_confirmation: values.password_confirmation,
      password: values.password,
      username: values.username,
      id: values.id,
      image: values.image,
      phone_number: values.phone_number,
    })
    if ('data' in res) {
    }
  }
  if (isLoading) {
    return <Loading />
  }
  if (error) {
    return <Error />
  }
  if (data?.data.admin)
    return (
      <Formik
        initialValues={{
          name: data.data.admin.name,
          username: data.data.admin.username,
          password_confirmation: '',
          password: '',
          id: data.data.admin.id,
          image: data.data.admin.image,
          phone_number: data.data.admin.phone_number,
        }}
        onSubmit={onSubmitHandler}
        component={({
          values,
          isSubmitting,
        }: FormikProps<ProfileFormValues>) => {
          return (
            <Form className="grid grid-cols-3 gap-8">
              <div className="flex h-full w-full items-center justify-center">
                <ImageFieldPro
                  name="image"
                  defaultImage={values.image.toString()}
                  asAvatar={true}
                />
              </div>
              <div className="col-span-2 grid w-full grid-cols-2 gap-4">
                <TextField
                  label="الاسم"
                  name="name"
                  type="text"
                  required={true}
                />
                <TextField
                  label="اسم المستخدم"
                  name="username"
                  type="text"
                  required={true}
                />
                <TextField
                  label="كلمة السر"
                  name="password"
                  type="password"
                  required={true}
                />
                <TextField
                  label="تأكيد كلمة السر"
                  name="password_confirmation"
                  type="password"
                  required={true}
                />
              </div>
              <div className="col-span-3 flex justify-end">
                <Button type="submit" isLoading={isSubmitting}>
                  حفظ التغيرات
                </Button>
              </div>
            </Form>
          )
        }}
      ></Formik>
    )
}

export default Profile
