import {
  PayloadAction,
  Slice,
  createAsyncThunk,
  createSlice,
} from '@reduxjs/toolkit'
import { NavigationProps } from '@/types'
import { navBarLists } from '@/utilities/navigation/navbar'

type TInitialState = {
  navigationRoutes: NavigationProps[]
}

const sliceName: string = 'shared'
const initState: TInitialState = {
  navigationRoutes: [],
}

export const getNavigationRoutes = createAsyncThunk(
  `${sliceName}/getNavigationRoutes`,
  () => {
    const res: NavigationProps[] = navBarLists
    return res
  },
)


const SharedSlice: Slice<TInitialState> = createSlice({
  name: sliceName,
  initialState: initState,
  reducers: {
    updateNavigationRoutes: (
      state: TInitialState,
      action: PayloadAction<NavigationProps[]>,
    ) => {
      state.navigationRoutes[1].list = action.payload
      return state
    },
  },
  extraReducers(builder) {
    builder.addCase(
      getNavigationRoutes.fulfilled,
      (state: TInitialState, action: PayloadAction<NavigationProps[]>) => {
        state.navigationRoutes = action.payload
      },
    )
  },
})

export const { updateNavigationRoutes } = SharedSlice.actions

export default SharedSlice.reducer
