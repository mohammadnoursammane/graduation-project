import { AdminData } from '@/types'
import { PayloadAction, Slice, createSlice } from '@reduxjs/toolkit'

type TInitialState = {
  userId: string
  userRole: string[]
}
const user: AdminData | null =
  typeof window !== 'undefined'
    ? localStorage.getItem('user') !== null
      ? JSON.parse(localStorage.getItem('user')!)
      : null
    : null
const sliceName: string = 'auth'

const AuthSlice: Slice<TInitialState> = createSlice({
  name: sliceName,
  initialState: {
    userId: user ? user.admin.id : '',
    userRole: user ? user.admin.role : [],
  },
  reducers: {
    updateUser: (state, action: PayloadAction<TInitialState>) => {
      state.userId = action.payload.userId
      state.userRole = action.payload.userRole
    },
  },
})

export const { updateUser } = AuthSlice.actions

export default AuthSlice.reducer
