import { configureStore } from '@reduxjs/toolkit'

import sharedSlice from './features/shared/sharedSlice'

import authSlice from './features/auth/authSlice'
import { baseApi } from '@/services/baseApi'
import { setupListeners } from '@reduxjs/toolkit/query'
export const makeStore = () => {
  return configureStore({
    reducer: {
      shared: sharedSlice,
      auth: authSlice,
      [baseApi.reducerPath]: baseApi.reducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(baseApi.middleware),
  })
}

// Infer the type of makeStore
export type AppStore = ReturnType<typeof makeStore>
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']

setupListeners(makeStore().dispatch)
