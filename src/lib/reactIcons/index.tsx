import { IconType } from 'react-icons'
import { AiOutlineQrcode } from 'react-icons/ai'
import { BsBook } from 'react-icons/bs'
import { MdOutlineNotificationsActive } from 'react-icons/md'
import {
  PiCalculatorLight,
  PiListChecks,
  PiUserCircleDuotone,
} from 'react-icons/pi'
import { TbBellRinging, TbShoppingCart, TbTemplate, TbUsers } from 'react-icons/tb'
import { Icon, Reserve, TagUser } from 'iconsax-react'
import { FiUsers } from 'react-icons/fi'
const iconComponents: Record<string, IconType | Icon> = {
  BsBook: BsBook,
  TbTemplate: TbTemplate,
  PiUserCircleDuotone: PiUserCircleDuotone,
  TbUsers: TbUsers,
  AiOutlineQrcode: AiOutlineQrcode,
  PiListChecks: PiListChecks,
  MdOutlineNotificationsActive: MdOutlineNotificationsActive,
  PiCalculatorLight: PiCalculatorLight,
  Reserve: Reserve,
  TagUser: TagUser,
  TbBellRinging: TbBellRinging,
  FiUsers: FiUsers,
  TbShoppingCart: TbShoppingCart,
  // Add other icon components here if needed
}

// Function to get the icon component based on the icon type/name
const GetIconComponent = (iconType: string): IconType | Icon => {
  // Check if the icon type exists in the mapping, otherwise return a default icon
  return iconComponents[iconType] || BsBook // You can change BsBook to your default icon component
}

export default GetIconComponent
