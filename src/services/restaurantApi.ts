import { RestaurantData, RestaurantFormValues, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const restaurantApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getRestaurants: builder.query<
      TResponse<RestaurantData, 'restaurants'>,
      { name: string; owner_name: string; perPage: number }
    >({
      query: ({ name, owner_name, perPage }) => {
        return {
          url: `${apiRoutes.restaurant.index}`,
          params: { name, owner_name, perPage: perPage * 20, page: 1 },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['Restaurants'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    getRestaurant: builder.query<
      TResponse<RestaurantData>,
      { id: string | number }
    >({
      query: ({ id }) => {
        return {
          url: `${apiRoutes.restaurant.show(id)}`,
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['Restaurants'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addRestaurant: builder.mutation<
      TResponse<RestaurantData>,
      RestaurantFormValues
    >({
      query: ({ name, address, description, image, owner_id, tax }) => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('address', address)
        formData.append('description', description)
        formData.append('image', image)
        formData.append('owner_id', owner_id!.toString())
        formData.append('tax', tax.toString())
        return {
          url: apiRoutes.restaurant.buttons.add,
          method: 'POST',
          body: formData,
        }
      },
    }),

    editRestaurant: builder.mutation<
      TResponse<RestaurantData>,
      RestaurantFormValues
    >({
      query: ({ name, id, address, description, image, owner_id, tax }) => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('address', address)
        formData.append('description', description)
        formData.append('image', image)
        formData.append('owner_id', owner_id!.toString())
        formData.append('tax', tax.toString())
        return {
          url: apiRoutes.restaurant.buttons.modify(id!),
          method: 'POST',
          body: formData,
        }
      },
    }),
    deleteRestaurant: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.restaurant.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useGetRestaurantQuery,
  useAddRestaurantMutation,
  useDeleteRestaurantMutation,
  useGetRestaurantsQuery,
  useEditRestaurantMutation,
} = restaurantApi
