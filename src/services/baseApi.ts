import { AdminData } from '@/types'
import {
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError,
  createApi,
  fetchBaseQuery,
} from '@reduxjs/toolkit/query/react'

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.HOST_API,
  prepareHeaders: (headers, { getState }: any) => {
    const user: AdminData = JSON.parse(localStorage.getItem('user') ?? '')
    const token = user.token
    // If we have a token set in state, let's assume that we should be passing it.
    if (token) {
      headers.set('Authorization', `Bearer ${token}`)
    }

    return headers
  },
})

const baseQueryWithReAuth: BaseQueryFn<
  string | FetchArgs,
  unknown,
  FetchBaseQueryError
> = async (args, api: any, extraOptions) => {
  let result = await baseQuery(args, api, extraOptions)
  const user: AdminData = JSON.parse(localStorage.getItem('user') ?? '')
  const token = user.token
  if (!token) {
    window.location.href = '/login'
  }
  return result
}

// initialize an empty api service that we'll inject endpoints into later as needed
export const baseApi = createApi({
  baseQuery: baseQueryWithReAuth,
  endpoints: () => ({}),
  refetchOnReconnect: true,
  refetchOnMountOrArgChange: true,
  tagTypes: [
    'Admins',
    'Restaurants',
    'ads',
    'owners',
    'categories',
    'products',
    'customers',
    'employee',
  ],
  serializeQueryArgs: ({ endpointName }) => {
    return endpointName
  },
})
