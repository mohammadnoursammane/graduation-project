import { OrderData, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const orderApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getOrders: builder.query<
      TResponse<OrderData, 'orders'>,
      {
        waiter_name: string
        perPage: number
        restaurant_name: string
        user_name: string
        status: string | number
      }
    >({
      query: ({ restaurant_name, status, user_name, waiter_name, perPage }) => {
        return {
          url: `${apiRoutes.order.index}`,
          params: {
            restaurant_name,
            status,
            user_name,
            waiter_name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    getOrder: builder.query<
      TResponse<{ order: OrderData }>,
      { id: string | number }
    >({
      query: ({ id }) => {
        return {
          url: `${apiRoutes.order.show(id)}`,
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    getOrdersUser: builder.query<
      TResponse<OrderData ,"orders">,
      { id: string | number }
    >({
      query: ({ id }) => {
        return {
          url: `${apiRoutes.order.allUsers(id)}`,
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
  }),
  overrideExisting: false,
})

export const { useGetOrderQuery, useGetOrdersQuery ,useGetOrdersUserQuery} = orderApi
