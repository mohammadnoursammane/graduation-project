import { EmployeeData, EmployeeFormValues, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const employeeApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getEmployees: builder.query<
      TResponse<EmployeeData, 'waiters'>,
      { restaurant_name: string; name: string; perPage: number }
    >({
      query: ({ restaurant_name, name, perPage }) => {
        return {
          url: `${apiRoutes.employee.index}`,
          params: {
            restaurant_name,
            name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['employee'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addEmployee: builder.mutation<TResponse<EmployeeData>, EmployeeFormValues>({
      query: ({
        image,
        restaurant_id,
        name,
        password,
        password_confirmation,
        phone_number,
        username,
      }) => {
        const formData = new FormData()
        formData.append('restaurant_id', restaurant_id!.toString())
        formData.append('image', image)
        formData.append('name', name)
        formData.append('password', password)
        formData.append('password_confirmation', password_confirmation)
        formData.append('phone_number', phone_number)
        formData.append('username', username)
        return {
          url: apiRoutes.employee.buttons.add,
          method: 'POST',
          body: formData,
        }
      },
    }),
    editEmployee: builder.mutation<TResponse<EmployeeData>, EmployeeFormValues>(
      {
        query: ({
          image,
          restaurant_id,
          name,
          password,
          password_confirmation,
          phone_number,
          username,
          id,
        }) => {
          const formData = new FormData()
          formData.append('restaurant_id', restaurant_id!.toString())
          formData.append('image', image)
          formData.append('name', name)
          formData.append('password', password)
          formData.append('password_confirmation', password_confirmation)
          formData.append('phone_number', phone_number)
          formData.append('username', username)

          return {
            url: apiRoutes.employee.buttons.modify(id!),
            method: 'POST',
            body: formData,
          }
        },
      },
    ),
    deleteEmployee: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.employee.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useAddEmployeeMutation,
  useDeleteEmployeeMutation,
  useGetEmployeesQuery,
  useEditEmployeeMutation,
} = employeeApi
