import { TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'
import { ProfileData, ProfileFormValues } from '@/types/auth'

const profileApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getProfile: builder.query<TResponse<{ admin: ProfileData }>, void>({
      query: () => `${apiRoutes.profile.index}`,
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },

      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),

    editProfile: builder.mutation<
      TResponse<{ admin: ProfileData }>,
      ProfileFormValues
    >({
      query: ({
        name,
        id,
        image,
        password_confirmation,
        phone_number,
        username,
        password,
      }) => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('image', image)
        formData.append('password_confirmation', password_confirmation)
        formData.append('phone_number', phone_number)
        formData.append('username', username)
        formData.append('password', password)
        return {
          url: apiRoutes.profile.buttons.modify,
          method: 'POST',
          body: formData,
        }
      },
    }),
  }),
  overrideExisting: false,
})

export const { useEditProfileMutation, useGetProfileQuery } = profileApi
