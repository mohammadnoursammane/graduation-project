import { CategoryData, CategoryFormValues, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const categoryApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getCategories: builder.query<
      TResponse<CategoryData, 'categories'>,
      { name: string; perPage: number; restaurantId: string }
    >({
      query: ({ name, perPage, restaurantId }) => {
        return {
          url: `${apiRoutes.category.index(restaurantId)}`,
          params: {
            name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['categories'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    getCategory: builder.query<
      TResponse<CategoryData>,
      { id: string | number }
    >({
      query: ({ id }) => {
        return {
          url: `${apiRoutes.category.show(id)}`,
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addCategory: builder.mutation<TResponse<CategoryData>, CategoryFormValues>({
      query: ({ name, restaurantId }) => {
        return {
          url: apiRoutes.category.buttons.add(restaurantId!),
          method: 'POST',
          body: { name: name },
        }
      },
    }),
    editCategory: builder.mutation<TResponse<CategoryData>, CategoryFormValues>(
      {
        query: ({ name, id }) => ({
          url: apiRoutes.category.buttons.modify(id!),
          method: 'POST',
          body: { name: name },
        }),
      },
    ),
    deleteCategory: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.category.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useGetCategoryQuery,
  useAddCategoryMutation,
  useDeleteCategoryMutation,
  useEditCategoryMutation,
  useGetCategoriesQuery,
} = categoryApi
