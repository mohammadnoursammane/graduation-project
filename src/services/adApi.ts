import { AdData, AdFormValues, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const adApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getAds: builder.query<
      TResponse<AdData, 'ad'>,
      { restaurant_name: string; owner_name: string; perPage: number }
    >({
      query: ({ restaurant_name, owner_name, perPage }) => {
        return {
          url: `${apiRoutes.ad.index}`,
          params: {
            restaurant_name,
            owner_name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['ads'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addAd: builder.mutation<TResponse<AdData>, AdFormValues>({
      query: ({ image, restaurant_id }) => {
        const formData = new FormData()
        formData.append('restaurant_id', restaurant_id!.toString())
        formData.append('image', image)
        return {
          url: apiRoutes.ad.buttons.add,
          method: 'POST',
          body: formData,
        }
      },
    }),

    deleteAd: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.ad.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const { useAddAdMutation, useDeleteAdMutation, useGetAdsQuery } = adApi
