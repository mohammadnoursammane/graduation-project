import {
  OwnerRestaurantFormValues,
  OwnerRestaurantData,
  TResponse,
} from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const ownerApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getOwners: builder.query<
      TResponse<OwnerRestaurantData, 'owners'>,
      { name: string; perPage: number }
    >({
      query: ({ name, perPage }) => {
        return {
          url: `${apiRoutes.owner.index}`,
          params: {
            name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['owners'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addOwner: builder.mutation<
      TResponse<OwnerRestaurantData>,
      OwnerRestaurantFormValues
    >({
      query: ({
        image,
        name,
        password,
        password_confirmation,
        phone_number,
        username,
      }) => {
        const formData = new FormData()
        formData.append('image', image)
        formData.append('name', name)
        formData.append('password', password)
        formData.append('password_confirmation', password_confirmation)
        formData.append('phone_number', phone_number)
        formData.append('username', username)
        return{
        url: apiRoutes.owner.buttons.add,
        method: 'POST',
        body: formData
      }},
    }),
    editOwner: builder.mutation<
      TResponse<OwnerRestaurantData>,
      OwnerRestaurantFormValues
    >({
      query: ({
        image,
        name,
        password,
        password_confirmation,
        phone_number,
        username,
        id,
      }) => {
        const formData = new FormData()
        formData.append('image', image)
        formData.append('name', name)
        formData.append('password', password)
        formData.append('password_confirmation', password_confirmation)
        formData.append('phone_number', phone_number)
        formData.append('username', username)
        return{
        url: apiRoutes.owner.buttons.modify(id!),
        method: 'POST',
        body: formData,
      }},
    }),
    deleteOwner: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.owner.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useAddOwnerMutation,
  useDeleteOwnerMutation,
  useEditOwnerMutation,
  useGetOwnersQuery,
} = ownerApi
