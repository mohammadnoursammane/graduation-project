import {
  CategoryData,
  CategoryFormValues,
  ProductData,
  ProductFormValues,
  TResponse,
} from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const productApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getProducts: builder.query<
      TResponse<ProductData, 'products'>,
      {
        name: string
        perPage: number
        restaurantId: string
        categoryId: string
        price: number
      }
    >({
      query: ({ name, perPage, restaurantId, categoryId, price }) => {
        return {
          url: `${apiRoutes.product.index(categoryId, restaurantId)}`,
          params: {
            name,
            perPage: perPage * 20,
            page: 1,
            price,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['products'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    getProduct: builder.query<TResponse<ProductData>, { id: string | number }>({
      query: ({ id }) => {
        return {
          url: `${apiRoutes.product.show(id)}`,
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addProduct: builder.mutation<TResponse<ProductData>, ProductFormValues>({
      query: ({
        name,
        details,
        image,
        is_shown,
        price,
        categoryId,
        restaurantId,
      }) => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('details', details)
        formData.append('image', image)
        formData.append('is_shown', is_shown.toString())
        formData.append('price', price.toString())
        return {
          url: apiRoutes.product.buttons.add(categoryId!, restaurantId!),
          method: 'POST',
          body: formData,
        }
      },
    }),
    editProduct: builder.mutation<TResponse<ProductData>, ProductFormValues>({
      query: ({ name, id, details, image, is_shown, price }) => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('details', details)
        formData.append('image', image)
        formData.append('is_shown', is_shown.toString())
        formData.append('price', price.toString())
        return {
          url: apiRoutes.product.buttons.modify(id!),
          method: 'POST',
          body: formData,
        }
      },
    }),
    deleteProduct: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.product.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useAddProductMutation,
  useDeleteProductMutation,
  useEditProductMutation,
  useGetProductQuery,
  useGetProductsQuery,
} = productApi
