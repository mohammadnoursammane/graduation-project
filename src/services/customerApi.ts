import {
  CustomerData,
  CustomerFormValues,
  EmployeeData,
  EmployeeFormValues,
  TResponse,
} from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const customerApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getCustomers: builder.query<
      TResponse<CustomerData, 'users'>,
      { name: string; perPage: number }
    >({
      query: ({ name, perPage }) => {
        return {
          url: `${apiRoutes.customer.index}`,
          params: {
            name,
            perPage: perPage * 20,
            page: 1,
          },
        }
      },
      // Only have one cache entry because the arg always maps to one string
      serializeQueryArgs: ({ endpointName }) => {
        return endpointName
      },
      providesTags: ['customers'], // Provides tag for invalidation
      // Refetch when the page arg changes
      forceRefetch({ currentArg, previousArg }) {
        return currentArg !== previousArg
      },
    }),
    addCustomer: builder.mutation<TResponse<CustomerData>, CustomerFormValues>({
      query: ({
        image,
        name,
        password,
        password_confirmation,
        phone_number,
        username,
      }) => {
        const formData = new FormData()
        formData.append('image', image)
        formData.append('name', name)
        formData.append('password', password)
        formData.append('password_confirmation', password_confirmation)
        formData.append('phone_number', phone_number)
        formData.append('username', username)
        return {
          url: apiRoutes.customer.buttons.add,
          method: 'POST',
          body: formData,
        }
      },
    }),
    editCustomer: builder.mutation<TResponse<CustomerData>, CustomerFormValues>(
      {
        query: ({
          image,
          name,
          password,
          password_confirmation,
          phone_number,
          username,
          id,
        }) => {
          const formData = new FormData()
          formData.append('image', image)
          formData.append('name', name)
          formData.append('password', password)
          formData.append('password_confirmation', password_confirmation)
          formData.append('phone_number', phone_number)
          formData.append('username', username)
          return {
            url: apiRoutes.customer.buttons.modify(id!),
            method: 'POST',
            body: formData,
          }
        },
      },
    ),
    deleteCustomer: builder.mutation<any, string | number>({
      query: (id) => ({
        url: apiRoutes.customer.buttons.delete(id),
        method: 'DELETE',
      }),
    }),
  }),
  overrideExisting: false,
})

export const {
  useAddCustomerMutation,
  useDeleteCustomerMutation,
  useEditCustomerMutation,
  useGetCustomersQuery,
} = customerApi
