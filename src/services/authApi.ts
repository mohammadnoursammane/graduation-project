import { AdminData, TResponse } from '@/types'
import { baseApi } from './baseApi'
import apiRoutes from '@/api'

const authApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    login: builder.mutation<
      TResponse<AdminData>,
      { password: string; username: string }
    >({
      query: ({ password, username }) => ({
        url: apiRoutes.auth.blockCodeGenerator,
        method: 'POST',
        body: JSON.stringify({ password, username }),
        headers: { 'Content-Type': 'application/json' },
      }),
    }),
  }),
  overrideExisting: false,
})

export const { useLoginMutation } = authApi
