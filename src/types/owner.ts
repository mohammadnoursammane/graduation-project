export type OwnerRestaurantData = {
  id: number
  name: string
  username: string
  phone_number: string
  created_at: Date
  role: string[]
  image: string
}

export type OwnerRestaurantFormValues = {
  name: string
  username: string
  password: string
  password_confirmation: string
  phone_number: string
  image: string | File
  id?: string | number
}
