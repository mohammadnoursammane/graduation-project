export type CustomerData = {
  id: number | string
  name: string
  username: string
  phone_number: string
  total_points: number
  image: string
}

export type CustomerFormValues = {
  id?: number | string
  name: string
  username: string
  phone_number: string
  image: string | File
  password: string
  password_confirmation: string
}
