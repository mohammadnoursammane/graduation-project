export type TResponse<
  DataType,
  KeyName extends string | undefined = undefined,
> = {
  success: boolean
  message: string
  data: KeyName extends string ? ArrayData<DataType, KeyName> : DataType
}
export type ArrayData<Data, KeyName extends string> = {
  [key in KeyName]: Data[]
} & { total: number }

import {
  ThemeColors,
  StyleTypes,
  TableProps,
  ModalStates,
  NavigationProps,
  SelectedField,
} from './shared'
import {
  LoginFormValues,
  AdminData,
  ProfileData,
  ProfileFormValues,
} from './auth'
import { RestaurantData, RestaurantFormValues } from './restaurant'
import { AdData, AdFormValues } from './ad'
import { EmployeeData, EmployeeFormValues } from './employee'
import { OwnerRestaurantData, OwnerRestaurantFormValues } from './owner'
import { CategoryData, CategoryFormValues } from './category'
import { ProductData, ProductFormValues } from './product'
import { OrderData } from './order'
import { CustomerData, CustomerFormValues } from './customer'
export type {
  ThemeColors,
  StyleTypes,
  LoginFormValues,
  TableProps,
  ModalStates,
  NavigationProps,
  SelectedField,
  AdminData,
  ProfileData,
  ProfileFormValues,
  RestaurantData,
  RestaurantFormValues,
  AdData,
  AdFormValues,
  EmployeeData,
  EmployeeFormValues,
  OwnerRestaurantData,
  OwnerRestaurantFormValues,
  CategoryData,
  CategoryFormValues,
  ProductData,
  ProductFormValues,
  OrderData,
  CustomerData,
  CustomerFormValues,
}
