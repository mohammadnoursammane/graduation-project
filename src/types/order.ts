export type OrderData = {
  id: number
  created_at: Date
  user_name: string
  waiter_name: string
  restaurant_name: string
  table_number: number
  status: number
  price: number
  restaurant_tax: number
  total_points: number
  total_price: number
  coupon_discount: string | number
  products: {
    id: number
    product_name: string
    product_price: number
    product_count: number
    product_details: string
    image: string
  }[]
}
