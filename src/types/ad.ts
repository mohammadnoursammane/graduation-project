export type AdData = {
  id: number
  image: string
  restaurant_name: string
  restaurant_owner_name: string
}
export type AdFormValues = {
  image: File | string
  restaurant_id: number | null
}
