export type ProductData = {
  id: number
  name: string
  price: number
  points: number
  details: string
  image: string
  is_shown: number
}

export type ProductFormValues = {
  name: string
  price: number
  details: string
  is_shown: number
  image: File | string
  id?: string | number
  restaurantId: string | number
  categoryId: string | number
}
