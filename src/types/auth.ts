
export type LoginFormValues = {
  username: string
  password: string
}
export type AdminData = {
  token: string
  admin: {
    id: string
    name: string
    username: string
    phone_number: string
    created_at: Date
    role: string[]
  }
}



export type ProfileData = {
  id: string
  name: string
  role: string[]
  image: string
  username: string
  phone_number: string
  created_at: Date
}

export type ProfileFormValues = {
  id: string | number
  name: string
  image: string | File
  username: string
  phone_number: string
  password_confirmation: string
  password: string
}
