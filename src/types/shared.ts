import { TableColumn } from 'react-data-table-component'

export type ThemeColors =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'dark'
  | 'gradient'
  | 'semiDark'
  | 'transparent'

export type StyleTypes = 'fill' | 'outline'

export type ModalStates =
  | 'delete'
  | 'add'
  | 'edit'
  | 'view'
  | 'reorder'
  | 'toggle'
  | null

export type TableProps<T = any> = {
  paginationProps?: {
    paginationPage: { activePage: number | string; perPage: number | string }
    paginationTotal: number
    setPaginationPage: React.Dispatch<
      React.SetStateAction<{ activePage: number; perPage: number }>
    >
  }
  setRefresh?: React.Dispatch<React.SetStateAction<boolean>>
  loading: boolean
  error: any
  data: T[]
  columns: TableColumn<T>[]
  setSortColumn?: React.Dispatch<React.SetStateAction<any>>
}

export type SelectedField = {
  id: string
  name: string
}

export type NavigationProps = {
  id: number
  titleLink?: string
  title?: string
  Icon?: any
  link?: string
  queryLink?: string
  pageTabs?: string[]
  list?: NavigationProps[]
  roles?: string[]
}
