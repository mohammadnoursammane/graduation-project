export type EmployeeData = {
  id: number
  name: string
  username: string
  phone_number: string
  restaurant_name: string
  orders_count: number
  image: string
}

export type EmployeeFormValues = {
  restaurant_id: number
  name: string
  username: string
  password: string
  password_confirmation: string
  phone_number: string
  image: File | string
  id?: string | number
}
