import { OwnerRestaurantData } from "./owner"

export type RestaurantData = {
  id: number
  name: string
  address: string
  description: string
  tax: number
  owner: OwnerRestaurantData
  products_count: number
  categories_count: number
  created_at: Date
  image: string
}


export type RestaurantFormValues = {
  id?: number
  name: string
  description: string
  address: string
  owner_id: number | null
  tax: number
  image: File | string
}
