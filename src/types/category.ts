export type CategoryData = { id: number; name: string; products_count: number }

export type CategoryFormValues = {
  name: string
  id?: number
  restaurantId?: number | string
}
