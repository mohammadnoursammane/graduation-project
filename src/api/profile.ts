const AddPrefix = (url: string) => 'admin' + url

const profile = {
  index: AddPrefix(``),
  buttons: {
    modify: AddPrefix(`/update`),
  },
}

export default profile
