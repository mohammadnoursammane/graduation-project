const AddPrefix = (url: string) => 'category' + url

const category = {
  index: (id: string | number) => AddPrefix(`/restaurant/${id}`),
  buttons: {
    add: (id: string | number) => AddPrefix(`/restaurant/${id}`),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
  show: (id: string | number) => AddPrefix(`/${id}`),
}

export default category
