const AddPrefix = (url: string) => '' + url

const auth = {
  login: AddPrefix(`/login`),
  refreshToken: AddPrefix(`/RefreshToken`),
  blockDashboardUser: AddPrefix('/ChangeDashboardBlockState'),
  blockCodeGenerator: AddPrefix('/ChangeCodeGeneratorBlockState'),
  blockStudent: AddPrefix('/ChangeStudentBlockState'),
}

export default auth
