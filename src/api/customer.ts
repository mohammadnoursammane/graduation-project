const AddPrefix = (url: string) => 'user' + url

const customer = {
  index: AddPrefix(`/`),
  buttons: {
    add: AddPrefix('/'),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
  coupon: AddPrefix('/coupon'),
}

export default customer
