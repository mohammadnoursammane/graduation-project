const AddPrefix = (url: string) => 'owner' + url

const owner = {
  index: AddPrefix(`/`),
  buttons: {
    add: AddPrefix('/'),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
}

export default owner
