const AddPrefix = (url: string) => 'ad' + url

const ad = {
  index: AddPrefix(`/`),
  buttons: {
    add: AddPrefix('/'),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
}

export default ad
