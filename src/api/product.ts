const AddPrefix = (url: string) => 'product' + url

const product = {
  index: (categoryId: string | number, restaurantId: string | number) =>
    AddPrefix(`/restaurant/${restaurantId}/category/${categoryId}`),
  buttons: {
    add: (categoryId: string | number, restaurantId: string | number) =>
      AddPrefix(`/restaurant/${restaurantId}/category/${categoryId}`),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
  show: (id: string | number) => AddPrefix(`/${id}`),
}

export default product
