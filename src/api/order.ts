const AddPrefix = (url: string) => 'order' + url

const order = {
  index: AddPrefix(``),
  allUsers:(id: string | number) =>AddPrefix(`/user/${id}`),
  show: (id: string | number) => AddPrefix(`/${id}`),
}

export default order
