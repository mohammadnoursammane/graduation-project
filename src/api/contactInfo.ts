const AddPrefix = (url: string) => '/contact' + url

const contactInfo = {
  index: AddPrefix(``),
  buttons: {
    add: AddPrefix(''),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
}

export default contactInfo
