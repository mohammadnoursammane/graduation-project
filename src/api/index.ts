import contactInfo from './contactInfo'
import auth from './auth'
import restaurant from './restaurant'
import ad from './ad'
import employee from './employee'
import owner from './owner'
import category from './category'
import product from './product'
import order from './order'
import profile from './profile'
import customer from './customer'
const apiRoutes = {
  contactInfo,
  restaurant,
  auth,
  ad,
  employee,
  owner,
  category,
  product,
  order,
  profile,
  customer,
}
export default apiRoutes
