const AddPrefix = (url: string) => 'restaurant' + url

const restaurant = {
  index: AddPrefix(`/`),
  buttons: {
    add: AddPrefix(''),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
  show: (id: string | number) => AddPrefix(`/${id}`),
}

export default restaurant
