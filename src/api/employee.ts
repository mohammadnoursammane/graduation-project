const AddPrefix = (url: string) => 'waiter' + url

const employee = {
  index: AddPrefix(`/`),
  buttons: {
    add: AddPrefix('/'),
    modify: (id: string | number) => AddPrefix(`/${id}/update`),
    delete: (id: string | number) => AddPrefix(`/${id}`),
  },
}

export default employee
