import { Owners } from '@/components/pages'
import { Metadata } from 'next'
import React, { Suspense } from 'react'

export const metadata: Metadata = {
  title: 'مستخدمو اللوحة',
}
function OwnersPage() {
  return (
    <Suspense>
      <Owners />
    </Suspense>
  )
}

export default OwnersPage
