import { PageContainer } from '@/components/containers'
import { Home } from '@/components/pages'
import { Metadata } from 'next'
import Head from 'next/head'
import React, { Suspense } from 'react'
export const metadata: Metadata = {
  title: 'الرئيسية',
}
function HomePage() {
  return (
    <Suspense>
      <Home />
    </Suspense>
  )
}

export default HomePage
