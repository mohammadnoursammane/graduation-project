import Categories from '@/components/pages/category'
import { Metadata } from 'next'
import React from 'react'
export const metadata: Metadata = {
  title: 'الأصناف',
}
function RestaurantPage({ params }: { params: { restaurantId: string } }) {
  return <Categories restaurantId={params.restaurantId} />
}

export default RestaurantPage
