import Products from '@/components/pages/product'
import { Metadata } from 'next';
import React from 'react'

export const metadata: Metadata = {
  title: 'المنتجات',
}
function CategoryPage({
  params,
}: {
  params: { restaurantId: string; categoryId: string }
}) {
  return (
    <Products
      categoryId={params.categoryId}
      restaurantId={params.restaurantId}
    />
  )
}

export default CategoryPage
