import Employees from '@/components/pages/employees'
import { Metadata } from 'next'
import { Suspense } from 'react'

export const metadata: Metadata = {
  title: 'الموظفين',
}
function EmployeesPage() {
  return (
    <Suspense>
      <Employees />
    </Suspense>
  )
}

export default EmployeesPage
