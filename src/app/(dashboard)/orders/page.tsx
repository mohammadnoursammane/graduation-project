import { Orders } from '@/components/pages'
import { Metadata } from 'next'
import React, { Suspense } from 'react'
export const metadata: Metadata = {
  title: 'الطلبية',
}
function OrderPage() {
  return (
    <Suspense>
      <Orders />
    </Suspense>
  )
}

export default OrderPage
