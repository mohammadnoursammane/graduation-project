import { ViewOrder } from '@/components/pages'
import { Metadata } from 'next'
import React from 'react'
export const metadata: Metadata = {
  title: 'تفاصيل الطلبية',
}
function OrderSelectedPage({ params }: { params: { orderId: string } }) {
  return <ViewOrder orderId={params.orderId} />
}

export default OrderSelectedPage
