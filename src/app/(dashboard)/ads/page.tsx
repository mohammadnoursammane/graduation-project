import Ads from '@/components/pages/ads'
import { Metadata } from 'next'
import { Suspense } from 'react'

export const metadata: Metadata = {
  title: 'الاعلانات',
}
function AdsPage() {
  return (
    <Suspense>
      <Ads />
    </Suspense>
  )
}

export default AdsPage