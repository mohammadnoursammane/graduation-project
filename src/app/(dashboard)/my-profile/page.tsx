import { Profile } from '@/components/pages'
import { Metadata } from 'next'
import React from 'react'

export const metadata: Metadata = {
  title: 'الملف الشخصي',
}
function ProfilePage() {
  return (
    <div className="container mx-auto p-4 py-10">
      <div className="w-full rounded bg-white p-4">
        <h2 className="border border-x-0 border-t-0 px-4 pb-2 mb-4 text-heading font-bold text-xl">الملف الشخصي</h2>
        <Profile />
      </div>
    </div>
  )
}

export default ProfilePage
