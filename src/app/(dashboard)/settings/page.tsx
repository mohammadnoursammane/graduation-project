import { Settings } from '@/components/pages'
import { Metadata } from 'next'
import React from 'react'

export const metadata: Metadata = {
  title: 'الإعدادات',
}

function SettingsPage() {
  return <Settings/>
}

export default SettingsPage
