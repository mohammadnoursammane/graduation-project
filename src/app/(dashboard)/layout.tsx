import { Navbar } from '@/components/ui/layout'
import React from 'react'

function DashboardLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <div className="bg-background min-h-screen">
      <Navbar />
      {children}
    </div>
  )
}

export default DashboardLayout
