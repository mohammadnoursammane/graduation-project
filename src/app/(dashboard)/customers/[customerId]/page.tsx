import OrdersCustomer from '@/components/pages/customers/view'
import { Metadata } from 'next'
import React from 'react'

export const metadata: Metadata = {
  title: 'طلبيات الزبون',
}
function OrderCustomerPage({ params }: { params: { customerId: string } }) {
  return <OrdersCustomer userId={params.customerId} />
}

export default OrderCustomerPage
