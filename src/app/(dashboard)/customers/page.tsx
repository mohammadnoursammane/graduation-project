import { Customers } from '@/components/pages'
import { Metadata } from 'next'
import React, { Suspense } from 'react'

export const metadata: Metadata = {
  title: 'الزبائن',
}
function CustomersPage() {
  return (
    <Suspense>
      <Customers />
    </Suspense>
  )
}

export default CustomersPage
