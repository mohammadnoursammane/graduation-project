import { Almarai } from 'next/font/google'
import 'rsuite/dist/rsuite-no-reset-rtl.min.css'
import '@/assets/style/style.min.css'
import './globals.css'
import { Metadata } from 'next'
import StoreProvider from './StoreProvider'
import { ProgressBar } from '@/components/ui/layout'
import { Suspense } from 'react'
import { SpeedInsights } from '@vercel/speed-insights/next'
const almarai = Almarai({
  // adjustFontFallback: true,
  subsets: ['arabic'],
  weight: ['400', '700', '800'],
})

export const metadata: Metadata = {
  title: { default: '', absolute: '', template: 'لوحة التحكم | %s' },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang="en" dir="rtl">
      <body className={almarai.className}>
        <StoreProvider>
          <SpeedInsights />
          {children}
          <Suspense>
            <ProgressBar />
          </Suspense>
        </StoreProvider>
      </body>
    </html>
  )
}
