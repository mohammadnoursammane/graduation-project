import { ProgressBar } from '@/components/ui/layout'
import { Metadata } from 'next'
import React from 'react'

export const metadata: Metadata = {
  title: { default: '', absolute: 'تسجيل الدخول' },
}

function LoginLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <main className="bg-background">
      {children}
      <ProgressBar />
    </main>
  )
}

export default LoginLayout
