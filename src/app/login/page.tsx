'use client'
import { BottomShapeLogin, Logo, TopShapeLogin } from '@/assets/svgs'
import { TextField } from '@/components/forms'
import { Button } from '@/components/ui/shared'
import { LoginFormValues } from '@/types'
import * as Yup from 'yup'
import { Form, Formik, FormikHelpers, FormikProps } from 'formik'
import validation from '@/utilities/custom_validation'
import apiRoutes from '@/api'
import { useRouter } from 'next/navigation'
import { useAppDispatch } from '@/hooks/ReduxHooks'
import { updateUser } from '@/lib/features/auth/authSlice'
import { useLoginMutation } from '@/services/authApi'
import LogoImage from '@/assets/images/logo.png'
import Image from 'next/image'
function LoginPage() {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const [login, {}] = useLoginMutation()
  const onSubmitHandler = async (
    values: LoginFormValues,
    {}: FormikHelpers<LoginFormValues>,
  ) => {
    const castingValues = JSON.stringify(values)
    const currentApiRoute = apiRoutes.auth.login
    const response = await login({
      password: values.password,
      username: values.username,
    })
    if ('data' in response) {
      localStorage.setItem('user', JSON.stringify(response.data.data))
      const userId = response.data.data.admin.id
      const userRole = response.data.data.admin.role
      dispatch(updateUser({ userId, userRole }))
      setTimeout(() => {
        router.replace('/')
      }, 0)
    }
  }
  const onValidationHandler = () => {
    return Yup.object().shape({
      username: validation.string({
        min: 2,
        requiredLabel: 'اسم المستخدم',
      }),
      password: validation.string({
        min: 4,
        requiredLabel: 'كلمة السر',
      }),
    })
  }
  return (
    <div className="relative flex h-screen w-full items-center justify-center">
      <div className="relative z-10 w-2/3 max-w-[450px]">
        <TopShapeLogin className="absolute left-0 top-0 -z-10 -translate-x-[20%] -translate-y-1/2" />
        <div className="flex w-full flex-col justify-center gap-8 rounded-md bg-white p-8 ">
          <div className="flex flex-col items-center gap-3">
            <Image
              src={LogoImage.src}
              height={100}
              width={100}
              className="w-64 object-cover"
              alt="logo"
            />
          </div>

          <Formik
            initialValues={{ username: '', password: '' }}
            onSubmit={onSubmitHandler}
            validationSchema={onValidationHandler}
          >
            {({ isValid, isSubmitting }: FormikProps<LoginFormValues>) => (
              <Form className="flex flex-col gap-4">
                <TextField
                  required={true}
                  label="معرف المستخدم"
                  name="username"
                  type="text"
                />
                <TextField
                  required={true}
                  label="كلمة السر"
                  name="password"
                  type="password"
                />
                <Button theme="primary" type="submit" isLoading={isSubmitting}>
                  تسجيل الدخول
                </Button>
              </Form>
            )}
          </Formik>
        </div>
        <BottomShapeLogin className="absolute bottom-0 right-0 -z-10 translate-x-1/3 translate-y-1/3" />
      </div>
    </div>
  )
}

export default LoginPage
