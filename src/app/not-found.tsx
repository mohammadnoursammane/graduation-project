import { Button } from '@/components/ui/shared'
import Link from 'next/link'

export default function NotFound() {
  return (
    <div className="flex flex-col h-[75dvh] items-center justify-center gap-8">
      <h2 className="text-3xl font-semibold text-heading">الصفحة غير موجودة</h2>
      <Button>
        <Link href="/">عودة للرئيسة</Link>
      </Button>
    </div>
  )
}
